// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcclient

import (
	"context"
	"errors"
	"io"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	"entanglement.garden/common/grpcx"
	"entanglement.garden/common/logging"
	"entanglement.garden/common/retry"
	"entanglement.garden/networking/client/config"
	"entanglement.garden/networking/client/networks"
	"entanglement.garden/networking/client/pki"
	"entanglement.garden/networking/protos"
)

var (
	conn              *grpc.ClientConn
	openwrtClient     protos.OpenWRTClient
	eventStreamClient protos.OpenWRT_EventStreamClient
	shutdown          = false
)

// Connect to the grpc server, retrying until Shutdown is called
func Connect(ctx context.Context) {
	log := logging.GetLog(ctx)
	retryer := retry.New()
	for {
		if shutdown {
			log.Debug("shutdown = true, not re-connecting")
			return
		}

		c := config.GetClientConfig()
		if c.GRPCServer == "" {
			log.Info("waiting for server address to be configured")
			retryer.Retry(ctx, errors.New("grpc server isn't configured"))
			continue
		}

		log.Debug("connecting to ", c.GRPCServer)
		if err := connect(ctx, c.GRPCServer); err != nil {
			retryer.Retry(ctx, err)
			continue
		}

		if err := processEvents(ctx); err != nil {
			retryer.Retry(ctx, err)
			continue
		}
	}
}

func connect(ctx context.Context, serverAddress string) error {
	log := logging.GetLog(ctx)

	log.Info("establishing grpc connection to ", serverAddress)
	var err error
	conn, err = grpcx.NewClientConnection(serverAddress, pki.PKI)
	if err != nil {
		log.Error("error dialing API server")
		return err
	}

	openwrtClient = protos.NewOpenWRTClient(conn)
	eventStreamClient, err = openwrtClient.EventStream(ctx, &protos.Empty{})
	if err != nil {
		log.Error("error getting event stream from openwrt client")
		return err
	}

	return nil
}

// Shutdown the grpc client
func Shutdown() error {
	shutdown = true
	if conn == nil {
		return nil
	}
	log.Info("shutting down grpc client")
	return conn.Close()
}

func processEvents(ctx context.Context) error {
	retryer := retry.New()
	for {
		if shutdown {
			return nil
		}

		event, err := eventStreamClient.Recv()
		if err != nil {
			if err == io.EOF {
				log.Debug("Got EOF from GRPC server")
			} else {
				log.Error("error receiving event from grpc stream: ", err)
			}

			return err
		}

		err = processEvent(eventStreamClient.Context(), event)
		if err != nil {
			retryer.Retry(ctx, err)
			continue
		}
	}
}

func processEvent(ctx context.Context, event *protos.Event) (err error) {
	log := logging.GetLog(ctx)

	switch {
	case event.Network != nil:
		log.Debugf("ensuring network state: %+v", event.Network)
		err = networks.EnsureNetwork(ctx, event.Network)

	case event.Host != nil:
		log.Debugf("ensuring host state: %+v", event.Host)
		err = networks.EnsureHost(event.Host)

	case event.PublicIp != nil:
		log.Debugf("ensuring public IP: %+v", event.PublicIp)
		err = networks.EnsurePublicIP(event.PublicIp)

	case event.PortForward != nil:
		log.Debugf("ensuring port forward: %+v", event.PortForward)
		err = networks.EnsurePortForward(event.PortForward)

	default:
		log.Debugf("ignoring event with no known actions: %+v", event)
	}

	if err != nil {
		log.Error("error handling event: ", err)
		return err
	}
	return nil
}
