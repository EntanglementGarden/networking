// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcclient

import (
	"context"

	"entanglement.garden/networking/protos"
)

func GetToken(hostname string) (string, error) {
	token, err := openwrtClient.GetToken(context.Background(), &protos.Instance{
		Hostname: hostname,
	})
	if err != nil {
		return "", err
	}

	return token.Token, nil
}
