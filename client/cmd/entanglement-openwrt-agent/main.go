// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/networking/client/common"
	"entanglement.garden/networking/client/config"
	"entanglement.garden/networking/client/grpcclient"
	"entanglement.garden/networking/client/localrpc"
	"entanglement.garden/networking/client/metadata"
	"entanglement.garden/networking/client/pki"
)

var signals = make(chan os.Signal, 1)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	common.ConfigureLogging()

	log.Info("Starting entanglement networking agent ", config.Version)

	config.AutoconfigUntilGRPCServerSet()

	if err := pki.Bootstrap(); err != nil {
		log.Fatal("error bootstrapping PKI: ", err)
	}

	if err := pki.BootstrapCert(); err != nil {
		log.Fatal("error configuring PKI: ", err)
	}

	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	if err := localrpc.ListenAndServe(); err != nil {
		log.Fatal("error starting local rpc server: ", err)
	}

	go metadata.ListenAndServe()
	go grpcclient.Connect(ctx)

	for {
		signal := <-signals
		log.Debug("received signal", signal)
		switch signal {
		case syscall.SIGINT, syscall.SIGTERM:
			cancel()

			if err := localrpc.Shutdown(); err != nil {
				log.Error("error shutting down local rpc server: ", err)
			}

			if err := metadata.Shutdown(); err != nil {
				log.Error("error shutting down metadata server: ", err)
			}

			if err := grpcclient.Shutdown(); err != nil {
				log.Error("error shutting down grpc client: ", err)
			}

			return
		}
	}

}
