// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package pki

import (
	"errors"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/common/grpcx"
	"entanglement.garden/networking/client/config"
)

var (
	PKI = &grpcx.PKI{
		Cert: stepPath + "/certs/openwrt.crt",
		Key:  stepPath + "/certs/openwrt.key",
		CA:   stepPath + "/certs/root_ca.crt",
	}
)

// Requests the ott token for the current instance and use it to issue a certificate
func BootstrapCert() error {
	if _, err := os.Stat(PKI.Cert); !os.IsNotExist(err) {
		return nil
	}

	token := config.GetClientConfig().StepOTT

	if token == "" {
		biosdata := config.ReadBiosData()
		token = biosdata.Get("t")
		if token == "" {
			return errors.New("no token source found")
		}
	}

	subject, err := getSubject(token)
	if err != nil {
		return fmt.Errorf("error parsing token: %v", err)
	}

	if stdout, err := runStepCommand("ca", "certificate", "--token", token, subject, PKI.Cert, PKI.Key); err != nil {
		if stdout != "" {
			log.Debug("step command failed with stdout: ", stdout)
		}

		return fmt.Errorf("error getting certificate: %v", err)
	}

	log.Info("local certificates configured in ", PKI.Cert)

	return nil
}
