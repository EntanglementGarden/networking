// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package pki

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"

	"github.com/golang-jwt/jwt"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/networking/client/config"
)

var (
	stepPath   = "/etc/step"
	rootCAPath string
)

func init() {
	p := os.Getenv("STEPPATH")
	if p != "" {
		stepPath = p
	}

	rootCAPath = stepPath + "/certs/root_ca.crt"
}

func Bootstrap() error {
	log.Debug("root ca path: ", rootCAPath)
	_, err := os.Stat(rootCAPath)
	if !os.IsNotExist(err) {
		return err
	}

	c := config.GetClientConfig()

	fingerprint := config.ReadBiosData().Get("f")
	if fingerprint == "" {
		fingerprint = c.StepFingerprint
	}

	log.Debug("bootstrapping step with server ", c.StepURL, " fingerprint ", fingerprint)
	if stdout, err := runStepCommand("ca", "bootstrap", "--ca-url", c.StepURL, "--fingerprint", fingerprint); err != nil {
		if stdout != "" {
			log.Debug("step command failed with stdout: ", stdout)
		}

		return fmt.Errorf("error bootstraping step CA: %v", err)
	}

	log.Debug("configured step locally")
	return nil
}

func runStepCommand(args ...string) (string, error) {
	return runStepCommandStdin(nil, args...)
}

func runStepCommandStdin(stdin io.Reader, args ...string) (string, error) {
	if _, err := os.Stat(stepPath); os.IsNotExist(err) {
		err = os.MkdirAll(stepPath, 0750)
		if err != nil {
			return "", err
		}
	}
	log.Debug("executing step ", args)

	output := bytes.Buffer{}

	cmd := exec.Command("step", args...)
	cmd.Env = append(cmd.Env, fmt.Sprintf("STEPPATH=%s", stepPath))
	cmd.Stdout = &output
	cmd.Stderr = log.StandardLogger().WriterLevel(log.WarnLevel)

	if stdin != nil {
		cmd.Stdin = stdin
	}

	if err := cmd.Run(); err != nil {
		return "", err
	}

	return output.String(), nil
}

func getSubject(tokenString string) (string, error) {
	jwtParser := jwt.Parser{}

	var claims jwt.MapClaims
	_, _, err := jwtParser.ParseUnverified(tokenString, &claims)
	if err != nil {
		return "", err
	}

	return claims["sub"].(string), nil
}
