// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"reflect"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	"entanglement.garden/common/cluster"
	"entanglement.garden/common/config"
	"entanglement.garden/common/httpx"
	"entanglement.garden/networking/client/uci"
)

const (
	biosDataFile = "/sys/class/dmi/id/product_serial"

	uciConfig  = "entanglement"
	uciSection = "entanglement"

	uciStructTag = "uci"

	dnsmasqOverrideFile = "/etc/dnsmasq-entanglement-overrides/bootstrap-dns.conf"
)

var (
	ErrNoAutoconfigSources = errors.New("no detectable network config sources")

	Version = config.Version
)

type Autoconfig struct {
	Config   ClientConfig `json:"config"`
	ExtraUCI uci.Tree     `json:"extra_uci,omitempty"`
}

// ProvisioningEndpoints defines model for ProvisioningEndpoints.
type ProvisioningEndpoints struct {
	LibvirtGRPC string `json:"libvirt_grpc"`
	OpenWRTGRPC string `json:"openwrt_grpc"`
	Step        string `json:"step"`
}

// ProvisioningInfo defines model for ProvisioningInfo.
type ProvisioningInfo struct {
	CAFingerprint string                `json:"ca_fingerprint"`
	Domain        string                `json:"domain"`
	Endpoints     ProvisioningEndpoints `json:"endpoints"`
}

type ClientConfig struct {
	GRPCServer      string `uci:"grpc_server" json:"grpc_server"`
	StepURL         string `uci:"step_url" json:"step_ca"`
	StepFingerprint string `uci:"step_fp" json:"step_fp"`
	StepOTT         string `uci:"step_ott" json:"step_ott"`
}

func (c ClientConfig) Commit() error {
	_, err := uci.UpdateTree(uci.Tree{
		uciConfig: {
			uciSection: {
				Type:   uciConfig,
				Values: c.getUCIValues(),
			},
		},
	})
	return err
}

func (c ClientConfig) getUCIValues() map[string]uci.Value {
	values := map[string]uci.Value{}

	elem := reflect.TypeOf(c)
	value := reflect.ValueOf(c)

	for i := 0; i < elem.NumField(); i++ {
		field := elem.Field(i)
		key := field.Tag.Get(uciStructTag)

		structValue := value.FieldByName(field.Name).Interface()
		if structValue == "" {
			continue // skip field with no tag for now
		}

		uciValue := uci.Value{}

		switch v := structValue.(type) {
		case string:
			uciValue.Value = v
		case []string:
			uciValue.List = v
		default:
			continue
		}

		values[key] = uciValue
	}

	return values
}

func GetClientConfig() (c ClientConfig) {
	elem := reflect.TypeOf(&c).Elem()
	value := reflect.ValueOf(&c).Elem()

	for i := 0; i < elem.NumField(); i++ {
		field := elem.Field(i)
		key := field.Tag.Get(uciStructTag)
		fieldValue := value.FieldByName(field.Name)

		if key == "" || !fieldValue.CanSet() {
			continue
		}

		switch fieldValue.Interface().(type) {
		case string:
			v := uci.Get(uciConfig, uciSection, key)
			fieldValue.SetString(v)
		case []string:
			v := uci.GetSlice(uciConfig, uciSection, key)
			fieldValue.Set(reflect.ValueOf(v))
		}
	}

	return c
}

func AutoconfigUntilGRPCServerSet() {
	for {
		if GetClientConfig().GRPCServer != "" {
			return
		}

		if err := autoconfig(); err != nil {
			logrus.Error("error bootstrapping config: ", err)

			if err == ErrNoAutoconfigSources {
				return
			}

			logrus.Debug("retrying in 5 seconds")
			time.Sleep(time.Second * 5)
			continue
		}
		break
	}
}

// autoconfig is a stupid name please suggest a better one
// it's basically cloud-init
func autoconfig() error {
	var config Autoconfig

	biosdata := ReadBiosData()

	// first check for biosdata key s, and if present query user data from the specified server and use it as the config
	// if s is not present, check for key c, and use the value as the JSON-encoded config
	bootstrapServerIP := biosdata.Get("s")
	if bootstrapServerIP != "" {
		configURL := "https://" + bootstrapServerIP + "/.well-known/rhyzome/provisioning.json"

		logrus.WithField("provisioning_url", configURL).Info("loading provisioning config")
		resp, err := httpx.GetInsecureHTTPClient(context.Background()).Get(configURL)
		if err != nil {
			return err
		}
		defer resp.Body.Close()

		var provisioning ProvisioningInfo
		if err := json.NewDecoder(resp.Body).Decode(&provisioning); err != nil {
			return err
		}

		if err := UpdateServerIP(provisioning.Domain, bootstrapServerIP); err != nil {
			return err
		}

		config.Config.GRPCServer = provisioning.Endpoints.OpenWRTGRPC
		config.Config.StepURL = provisioning.Endpoints.Step
		config.Config.StepFingerprint = provisioning.CAFingerprint
		cluster.Domain = provisioning.Domain
	} else {
		configstring := biosdata.Get("c")
		if configstring == "" {
			return ErrNoAutoconfigSources
		}

		configBytes, err := base64.StdEncoding.DecodeString(configstring)
		if err != nil {
			return err
		}

		err = json.Unmarshal(configBytes, &config)
		if err != nil {
			return fmt.Errorf("error parsing config from bios: %v", err)
		}
	}

	err := config.Config.Commit()
	if err != nil {
		return fmt.Errorf("error committing config: %v", err)
	}

	_, err = uci.UpdateTree(config.ExtraUCI)
	if err != nil {
		return fmt.Errorf("error committing extra UCI config: %v", err)
	}

	if _, ok := config.ExtraUCI["dhcp"]; ok {
		err := exec.Command("/etc/init.d/dnsmasq", "restart").Run()
		if err != nil {
			logrus.Error("error restrating dnsmasq: ", err)
		}
	}

	return nil
}

// UpdateServerIP sets the IP we should use to contact the gRPC and step server.
func UpdateServerIP(domain, ip string) error {
	confLines := []string{}
	for _, subdomain := range []string{"entanglement-garden-networking", "ca"} {
		confLines = append(confLines, fmt.Sprintf("address=/%s.%s/%s", subdomain, domain, ip))
	}

	desired := strings.Join(confLines, "\n")

	current, err := os.ReadFile(dnsmasqOverrideFile)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	if string(current) == desired {
		logrus.Debug("no changes needed to fake DNS entries")
		return nil // no change needed
	}

	logrus.Debug("updating fake DNS entries to ", ip)

	err = os.WriteFile(dnsmasqOverrideFile, []byte(desired), 0644)
	if err != nil {
		return err
	}

	if err := exec.Command("/etc/init.d/dnsmasq", "restart").Run(); err != nil {
		return err
	}

	return nil
}

func ReadBiosData() url.Values {
	configbytes, err := os.ReadFile(biosDataFile)
	if err != nil {
		logrus.Warn("error reading bios data: ", err)
		return url.Values{}
	}

	values, err := url.ParseQuery(string(configbytes))
	if err != nil {
		logrus.Warn("error parsing bios data: ", err)
		return url.Values{}
	}

	return values
}
