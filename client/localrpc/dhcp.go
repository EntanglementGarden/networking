// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package localrpc

import (
	log "github.com/sirupsen/logrus"
)

type DHCPAction string

const (
	DHCPActionAdd    DHCPAction = "add"
	DHCPActionUpdate DHCPAction = "update"
)

type DHCPScriptCallback struct {
	Action   DHCPAction
	MACAddr  string
	IPAddr   string
	Hostname string
}

type DHCPScriptServer int

func (d *DHCPScriptServer) Call(args *DHCPScriptCallback, reply *int) error {
	log.Debugf("DHCPScriptServer called: %+v", *args)
	switch args.Action {
	case DHCPActionAdd, DHCPActionUpdate:
		// ipam.Update(args.MACAddr, args.IPAddr, args.Hostname)
	}
	return nil
}
