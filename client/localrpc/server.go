// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package localrpc

import (
	"net"
	"net/rpc"
	"os"
	"path"

	log "github.com/sirupsen/logrus"
)

const (
	SocketPath = "/var/run/rhyzome/localrpc.sock"
)

var listener net.Listener

// ListenAndServe creates a listener on the unix socket and starts the server in a goroutine
func ListenAndServe() error {
	s := rpc.NewServer()

	err := s.Register(new(DHCPScriptServer))
	if err != nil {
		log.Fatal("error registering rpc: ", err)
	}

	err = os.Mkdir(path.Dir(SocketPath), 0750)
	if err != nil && !os.IsExist(err) {
		return err
	}

	err = os.Remove(SocketPath)
	if err != nil && !os.IsNotExist(err) {
		return err
	}

	listener, err = net.Listen("unix", SocketPath)
	if err != nil {
		return err
	}

	go s.Accept(listener)
	return nil
}

// Shutdown shuts down the server
func Shutdown() error {
	if err := listener.Close(); err != nil {
		return err
	}

	return nil
}
