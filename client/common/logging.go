// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package common

import (
	"fmt"
	"log/syslog"
	"runtime"
	"strings"

	"github.com/sirupsen/logrus"
	lSyslog "github.com/sirupsen/logrus/hooks/syslog"

	"entanglement.garden/common/config"
)

// ConfigureLogging configures logrus for syslog output
func ConfigureLogging() {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetReportCaller(true)
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp:             true,
		EnvironmentOverrideColors: true,
		CallerPrettyfier:          logPrettifier,
	})

	hook, err := lSyslog.NewSyslogHook("", "", syslog.LOG_INFO, "entanglement-networking")
	if err != nil {
		logrus.Error("error configuring syslog hook: ", err)
	}
	logrus.AddHook(hook)

	logger := logrus.WithFields(logrus.Fields{"service": config.ServiceName, "version": config.Version})
	logger.Info("service starting")
}

func logPrettifier(f *runtime.Frame) (string, string) {
	filenameParts := strings.SplitN(f.File, "/networking/", 2)
	filename := f.File
	if len(filenameParts) > 1 {
		filename = fmt.Sprintf("%s:%d", filenameParts[1], f.Line)
	}
	s := strings.Split(f.Function, ".")
	funcname := fmt.Sprintf("%s()", s[len(s)-1])
	return funcname, filename
}
