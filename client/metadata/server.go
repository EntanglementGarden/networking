// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package metadata

import (
	"context"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/networking/protos"
)

var (
	server *http.Server
	hosts  = map[string]HostData{} // map key is IP address
)

type HostData struct {
	InstanceID string
	NetworkID  string
	UserData   []byte
}

// ListenAndServe starts the metadata server
func ListenAndServe() {
	r := http.NewServeMux()
	r.HandleFunc("/user-data", cloudInitUserData)
	r.HandleFunc("/meta-data", cloudInitMetaData)
	r.HandleFunc("/vendor-data", cloudInitVendorData)
	// r.HandleFunc("/pki-token", StepToken) // TODO

	server = &http.Server{
		Addr:    "169.254.169.254:80",
		Handler: LoggingMiddleware(r),
	}

	log.Println("Starting metadata server on", server.Addr)
	err := server.ListenAndServe()
	if err != http.ErrServerClosed {
		log.Fatal("error running metadata server: ", err)
	}
}

// SetHostData stores data for a particular instance
func SetHostData(host *protos.Host) {
	log.Debug("storing data for ", host.Ip)
	hosts[host.Ip] = HostData{
		InstanceID: host.Instance,
		NetworkID:  host.NetworkId,
		UserData:   host.UserData,
	}
}

// Shutdown gracefully shuts down the metadata server
func Shutdown() error {
	log.Println("Shutting down metadata server")
	return server.Shutdown(context.Background())
}

// a wrapper around http.ResponseWriter to include data we want to log
type wrapper struct {
	http.ResponseWriter
	written int
	status  int
}

func (w *wrapper) WriteHeader(code int) {
	w.status = code
	w.ResponseWriter.WriteHeader(code)
}

func (w *wrapper) Write(b []byte) (int, error) {
	n, err := w.ResponseWriter.Write(b)
	w.written += n
	return n, err
}

// LoggingMiddleware is a middleware function that logs requests as they come in
func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		res := &wrapper{w, 0, 0}
		start := time.Now()
		next.ServeHTTP(res, r)
		log.Debug(r.RemoteAddr, " ", r.Method, " ", r.RequestURI, " ", res.status, " ", res.written, " ", time.Since(start))
	})
}
