// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package metadata

import (
	"net"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

func cloudInitUserData(w http.ResponseWriter, r *http.Request) {
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	data, ok := hosts[ip]
	if !ok {
		_, _ = w.Write([]byte{})
		return
	}

	_, _ = w.Write(data.UserData)
}

func cloudInitMetaData(w http.ResponseWriter, r *http.Request) {
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	data, ok := hosts[ip]
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/yaml")
	err := yaml.NewEncoder(w).Encode(map[string]string{
		"local-hostname": data.InstanceID,
		"instance-id":    data.InstanceID,
		"cloud-name":     "entanglement.garden",
	})
	if err != nil {
		log.Error("Error encoding response", err)
		http.Error(w, err.Error(), 500)
	}
}

func cloudInitVendorData(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte{})
}
