// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package networks

import (
	"fmt"
	"os/exec"

	"entanglement.garden/networking/client/uci"
	"entanglement.garden/networking/protos"
)

var interfaceWAN = "eth0"

func EnsurePublicIP(ip *protos.PublicIP) error {
	id := getUCIIdentifier(ip.Id)

	if uci.Get(uci.ConfigNetwork, id, "proto") == "static" {
		return nil // IP is probably already configured
	}

	changed, err := uci.UpdateTree(uci.Tree{
		uci.ConfigNetwork: {
			id: {
				Type: "interface",
				Values: map[string]uci.Value{
					"proto":   {Value: "static"},
					"device":  {Value: interfaceWAN},
					"ipaddr":  {Value: ip.Address},
					"netmask": {Value: ip.Netmask},
					"gateway": {Value: ip.Gateway},
				},
			},
		},
		uci.ConfigFirewall: {
			id: {
				Type: "zone",
				Values: map[string]uci.Value{
					"name":    {Value: id},
					"network": {List: []string{id}},
					"input":   {Value: "REJECT"},
					"output":  {Value: "ACCEPT"},
					"forward": {Value: "REJECT"},
					"masq":    {Value: "1"},
					"mtu_fix": {Value: "1"},
				},
			},
			id + "_icmp": {
				Type: "rule",
				Values: map[string]uci.Value{
					"src":       {Value: id},
					"proto":     {Value: "icmp"},
					"icmp_type": {Value: "echo-request"},
					"family":    {Value: "ipv4"},
					"target":    {Value: "ACCEPT"},
				},
			},
		},
	})
	if err != nil {
		return err
	}

	if changed {
		if err := exec.Command("/etc/init.d/network", "reload").Run(); err != nil {
			return fmt.Errorf("error reloading network service: %v", err)
		}

		if err := exec.Command("/etc/init.d/firewall", "reload").Run(); err != nil {
			return fmt.Errorf("error reloading firewall service: %v", err)
		}
	}

	return nil
}
