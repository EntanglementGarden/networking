// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package networks

import (
	"context"
	"fmt"
	"net"
	"os"
	"os/exec"
	"strings"

	"entanglement.garden/networking/client/uci"
	"entanglement.garden/networking/protos"
	log "github.com/sirupsen/logrus"
)

const interfaceVlan = "eth1"

func EnsureNetwork(ctx context.Context, network *protos.Network) error {
	// prepare some values
	ip, subnet, err := net.ParseCIDR(network.Subnet)
	if err != nil {
		return fmt.Errorf("error parsing requested subnet %s: %v", network.Subnet, err)
	}

	id := getUCIIdentifier(network.ResourceId)

	// create empty DHCP host list
	if err := writeLeases(id); err != nil {
		return fmt.Errorf("error creating %s: %v", dhcpHostsFilePrefix+network.ResourceId, err)
	}

	netInterface := fmt.Sprintf("%s.%d", interfaceVlan, network.VlanId)
	dnsmasqSectionName := fmt.Sprintf("dns_%s", id)
	zoneName := fmt.Sprintf("zone_%s", id)

	// serverWgKey, err := wgtypes.GeneratePrivateKey()
	// if err != nil {
	// 	return err
	// }

	tree := uci.Tree{
		"network": map[string]uci.Section{
			id: {
				Type: "interface",
				Values: map[string]uci.Value{
					"ifname":  {Value: netInterface},
					"proto":   {Value: "static"},
					"ipaddr":  {Value: ip.String()},
					"netmask": {Value: getMask(subnet)},
				},
			},
			// "wg_" + id: {
			// 	Type: "interface",
			// 	Values: map[string]uci.Value{
			// 		"proto":       {Value: "wireguard"},
			// 		"private_key": {Value: wgtypes.Key.String(serverWgKey)},
			// 		"listen_port": {Value: "51820"},
			// 		"addresses":   {Value: "10.0.5.1/24"},
			// 		"mtu":         {Value: "1420"},
			// 	},
			// },
		},
		"dhcp": map[string]uci.Section{
			id: {
				Type: "dhcp",
				Values: map[string]uci.Value{
					"interface":   {Value: id},
					"instance":    {Value: dnsmasqSectionName},
					"start":       {Value: "10"},
					"limit":       {Value: "240"},
					"leasetime":   {Value: "12h"},
					"dhcpv4":      {Value: "server"},
					"dynamicdhcp": {Value: "0"},
					// "dhcpv6": {Value: "server"},
					// "ra": {Value: "server"},
					// "ra_slaac": {Value: "1"},
				},
			},
			dnsmasqSectionName: {
				Type: "dnsmasq",
				Values: map[string]uci.Value{
					"rebind_protection": {Value: "0"},
					"readethers":        {Value: "0"},
					"interface":         {List: []string{id}},
					"notinterface":      {List: []string{"loopback"}},
					"dhcphostsfile":     {Value: dhcpHostsFilePrefix + id},
					"server":            {List: network.DnsServers},
				},
			},
		},
		"firewall": map[string]uci.Section{
			zoneName: {
				Type: "zone",
				Values: map[string]uci.Value{
					"name":    {Value: zoneName},
					"network": {List: []string{id}},
					"input":   {Value: "ACCEPT"},
					"forward": {Value: "ACCEPT"},
					"output":  {Value: "ACCEPT"},
				},
			},
		},
	}

	if network.EgressIp != "" {
		tree["firewall"]["forward_"+id] = uci.Section{
			Type: "forwarding",
			Values: map[string]uci.Value{
				"src":  {Value: zoneName},
				"dest": {Value: getUCIIdentifier(network.EgressIp)},
			},
		}
	}

	changed, err := uci.UpdateTree(tree)
	if err != nil {
		return err
	}

	if changed {
		log.Debug("reloading networking service")
		if err = reloadNetwork(); err != nil {
			return err
		}
	}

	return nil
}

func Delete(ctx context.Context, id string, vlanid int32) error {
	logger := log.WithFields(log.Fields{"network": id})
	logger.Info("deleting network")

	if err := os.Remove(dhcpHostsFilePrefix + id); err != nil {
		if !os.IsNotExist(err) {
			return err
		}
	}

	err := uci.Delete(uci.ConfigNetwork, id)
	if err != nil {
		return fmt.Errorf("error committing UCI: %v", err)
	}

	if err = reloadNetwork(); err != nil {
		return err
	}

	return nil
}

func reloadNetwork() error {
	if err := exec.Command("/etc/init.d/network", "reload").Run(); err != nil {
		return fmt.Errorf("error reloading network service: %v", err)
	}

	if err := exec.Command("/etc/init.d/dnsmasq", "reload").Run(); err != nil {
		return fmt.Errorf("error reloading dnsmasq service: %v", err)
	}

	if err := exec.Command("/etc/init.d/firewall", "reload").Run(); err != nil {
		return fmt.Errorf("error reloading firewall service: %v", err)
	}

	return nil
}

func getMask(subnet *net.IPNet) string {
	return fmt.Sprintf("%d.%d.%d.%d", subnet.Mask[0], subnet.Mask[1], subnet.Mask[2], subnet.Mask[3])
}

func getUCIIdentifier(s string) string {
	return strings.ReplaceAll(s, "-", "_")
}
