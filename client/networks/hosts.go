// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package networks

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
	"sync"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/common/cluster"
	"entanglement.garden/networking/client/config"
	"entanglement.garden/networking/client/metadata"
	"entanglement.garden/networking/protos"
)

const dhcpHostsFilePrefix = "/var/etc/dnsmasq-hosts-"

// expected lease state map[network]map[mac]ip
var (
	leases     map[string]map[string]*protos.Host = make(map[string]map[string]*protos.Host)
	leasesLock sync.Mutex
)

func EnsureHost(host *protos.Host) error {
	changed := ensureLease(host)

	if changed {
		if err := writeLeases(host.NetworkId); err != nil {
			return err
		}
	}

	metadata.SetHostData(host)

	if changed && host.ServicesHost {
		if err := config.UpdateServerIP(cluster.Domain, host.Ip); err != nil {
			return err
		}
	}

	return nil
}

func ensureLease(host *protos.Host) bool {
	leasesLock.Lock()
	defer leasesLock.Unlock()

	network := getUCIIdentifier(host.NetworkId)

	if _, ok := leases[network]; !ok {
		leases[network] = map[string]*protos.Host{}
	}

	if leases[network][host.Mac] == host {
		return false
	}

	log.Info("adding new DHCP lease for ", host.Mac, " -> ", host.Ip, " for network ", host.NetworkId)

	leases[network][host.Mac] = host

	return true
}

// writeLeases expects a network identifiers that has already been passed through getUCIIdentifier
func writeLeases(network string) error {
	network = getUCIIdentifier(network)

	filename := dhcpHostsFilePrefix + network
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	leasesLock.Lock()
	defer leasesLock.Unlock()

	for _, host := range leases[network] {
		// per dnsmasq docs, the format here is:
		// [<hwaddr>][,id:<client_id>|*][,set:<tag>][,tag:<tag>][,<ipaddr>][,<hostname>][,<lease_time>][,ignore]
		line := []string{host.Mac, host.Ip, host.Instance}
		_, err := f.WriteString(strings.Join(line, ",") + "\n")
		if err != nil {
			return fmt.Errorf("error writing %s: %v", filename, err)
		}
	}

	if err := f.Close(); err != nil {
		return err
	}

	log.Debug("wrote ", len(leases[network]), " leases to ", filename, ", reloading dnsmasq")

	cmd := exec.Command("/etc/init.d/dnsmasq", "reload")
	cmd.Stderr = log.StandardLogger().WriterLevel(log.WarnLevel)
	cmd.Stdout = log.StandardLogger().WriterLevel(log.DebugLevel)

	if err := cmd.Run(); err != nil {
		return fmt.Errorf("error reloading dnsmasq: %v", err)
	}

	return nil
}
