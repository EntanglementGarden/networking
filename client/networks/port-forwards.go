// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package networks

import (
	"fmt"
	"os/exec"
	"strconv"

	"entanglement.garden/networking/client/uci"
	"entanglement.garden/networking/protos"
)

func EnsurePortForward(fwd *protos.PortForward) error {
	id := getUCIIdentifier(fwd.Id)

	destPort := strconv.Itoa(int(fwd.InternalPort))
	changed, err := uci.UpdateTree(uci.Tree{
		uci.ConfigFirewall: {
			id: {
				Type: "redirect",
				Values: map[string]uci.Value{
					"src":       {Value: getUCIIdentifier(fwd.PublicIpId)},
					"src_dip":   {Value: fwd.PublicIp},
					"src_dport": {Value: strconv.Itoa(int(fwd.PublicPort))},
					"dest":      {Value: "zone_" + getUCIIdentifier(fwd.InternalNetwork)},
					"dest_ip":   {Value: fwd.InternalHost},
					"dest_port": {Value: destPort},
					"target":    {Value: "DNAT"},
				},
			},
		},
	})
	if err != nil {
		return err
	}

	if changed {
		if err := exec.Command("/etc/init.d/firewall", "reload").Run(); err != nil {
			return fmt.Errorf("error reloading firewall service: %v", err)
		}
	}

	return nil
}
