module entanglement.garden/networking/client

go 1.19

replace entanglement.garden/networking/protos => ../protos

require (
	entanglement.garden/common v0.0.0-20231208203011-089e7d4f2ba5
	entanglement.garden/networking/protos v0.0.0-00010101000000-000000000000
	github.com/digineo/go-uci v0.0.0-20210918132103-37c7b10c14fa
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/sirupsen/logrus v1.9.3
	google.golang.org/grpc v1.56.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/fsnotify/fsnotify v1.7.0 // indirect
	github.com/golang-jwt/jwt/v4 v4.4.2 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/uuid v1.3.1 // indirect
	github.com/labstack/echo/v4 v4.11.1 // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/ory/keto/proto v0.11.1-alpha.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/crypto v0.13.0 // indirect
	golang.org/x/net v0.12.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto v0.0.0-20230410155749-daa745c078e1 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
