// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package uci

type Tree map[ConfigName]map[string]Section

type Section struct {
	Type   string
	Remove bool
	Values map[string]Value
}

type Value struct {
	Value string
	List  []string `json:",omitempty"`
}

type ConfigName string

const (
	ConfigNetwork  = ConfigName("network")
	ConfigFirewall = ConfigName("firewall")
	ConfigDHCP     = ConfigName("dhcp")

	defaultTreePath = "/etc/config"
)
