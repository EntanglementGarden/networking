// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package uci

import (
	"fmt"
	"os"
	"path"
	"strings"

	uci "github.com/digineo/go-uci"
	log "github.com/sirupsen/logrus"
)

func UpdateTree(t Tree) (bool, error) {
	var u = uci.NewTree(defaultTreePath)

	log.Debug("ensuring uci values: ", t)

	changed := false

	for sectionName, config := range t {
		configChanged := false

		configName := string(sectionName)

		if _, err := os.Stat(path.Join(uci.DefaultTreePath, configName)); !os.IsNotExist(err) {
			// the uci library docs say this happens automatically, but I ran into some weirdness where it was silently failing to autoload,
			// causing the Commit() call to wipe out the existing config.
			if err := u.LoadConfig(configName, true); err != nil && !os.IsNotExist(err) {
				return false, fmt.Errorf("error loading uci config %s: %v", configName, err)
			}
		}

		for sectionName, section := range config {

			if section.Remove {
				log.Debug("deleting section ", configName, ".", sectionName)
				u.DelSection(configName, sectionName)
				configChanged = true
				continue
			}

			// no-op if the section already exists
			if err := u.AddSection(configName, sectionName, section.Type); err != nil {
				return false, fmt.Errorf("error adding UCI section %s.%s: %v", configName, sectionName, err)
			}

			for k, v := range section.Values {
				current, exists := u.Get(configName, sectionName, k)

				if exists {

					// check if the new value is a single string and matches the current value
					if v.Value != "" && len(current) == 1 && v.Value == current[0] {
						log.Debug("uci change not needed: ", configName, ".", sectionName, ".", k, " = ", current)
						continue
					}

					// check if the new value is a list and the has the same number of elements as the current value
					if len(v.List) > 0 && len(v.List) == len(current) {

						// iterate through each item in the list of new value and compare it with the the same position for the current value
						matches := true
						for i, item := range v.List {
							if current[i] == item {
								continue
							}

							matches = false
							break
						}

						if matches {
							log.Debug("uci change not needed: ", configName, ".", sectionName, ".", k, " = ", current)
							continue
						}
					}
				}

				success := false
				if v.Value != "" {
					log.Debug("updating UCI value ", configName, ".", sectionName, ".", k, "=", v.Value)
					success = u.SetType(configName, sectionName, k, uci.TypeOption, v.Value)
				} else {
					log.Debug("updating UCI value ", configName, ".", sectionName, ".", k, "=", v.List)
					success = u.SetType(configName, sectionName, k, uci.TypeList, v.List...)
				}

				if !success {
					return false, fmt.Errorf("error setting uci value %v.%v.%v", configName, sectionName, k)
				}

				configChanged = true
			}
		}

		if configChanged {
			if err := u.Commit(); err != nil {
				return false, fmt.Errorf("error committing uci changes: %v", err)
			}
			changed = true
		}
	}

	return changed, nil
}

func Delete(config ConfigName, section string) error {
	uci.DelSection(string(config), section)
	return uci.Commit()
}

func (u Value) String() string {
	if u.Value != "" {
		return u.Value
	}

	return strings.Join(u.List, ",")
}

func Get(config ConfigName, section string, option string) string {
	value := GetSlice(config, section, option)
	if len(value) == 0 {
		return ""
	}
	return value[0]
}

func GetSlice(config ConfigName, section string, option string) []string {
	value, _ := uci.Get(string(config), section, option)
	return value
}
