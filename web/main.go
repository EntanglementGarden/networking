// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"net/http"

	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
	"github.com/sirupsen/logrus"
)

var p = plugin.Plugin{
	Name:      "Networking",
	Extension: "entanglement.garden/networking",
	Endpoints: map[string]map[string]plugin.Endpoint{
		"/":    {http.MethodGet: plugin.AlwaysRedirect("/net")},
		"/net": {http.MethodGet: ListPage},
		"/net/create": {
			http.MethodGet:  CreatePage,
			http.MethodPost: CreatePage,
		},
		"/net/:id": {http.MethodGet: GetPage},

		"/public": {http.MethodGet: PublicSubnetsList},
		"/public/create": {
			http.MethodGet:  PublicSubnetCreate,
			http.MethodPost: PublicSubnetCreate,
		},
		"/public/:subnet": {
			http.MethodGet:  PublicIPsList,
			http.MethodPost: PublicIPCreate,
		},
		"/public/:subnet/:address": {
			http.MethodGet:  PublicIPPage,
			http.MethodPost: AddPortForward,
		},
	},
	Menu: &plugin_protos.MenuItem{
		Name: "Networking",
		Path: "/",
		Subitems: []*plugin_protos.MenuItem{
			{
				Name: "Networks",
				Path: "/net",
				Subitems: []*plugin_protos.MenuItem{
					{Name: "List", Path: ""},
					{Name: "Create", Path: "/create"},
				},
			},
			{
				Name: "Public IPs",
				Path: "/public",
				Subitems: []*plugin_protos.MenuItem{
					{Name: "List", Path: ""},
					{Name: "Create", Path: "/create"},
				},
			},
		},
	},
}

func main() {
	logrus.SetLevel(logrus.DebugLevel)
	p.ListenAndServe()
}
