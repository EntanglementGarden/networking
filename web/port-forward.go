// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/networking/web/openapi"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
)

func AddPortForward(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	form, err := url.ParseQuery(request.Body)
	if err != nil {
		log.Debug("error parsing request form: ", err)
		return nil, err
	}

	internalPort, err := strconv.ParseInt(form.Get("internal-port"), 10, 32)
	if err != nil {
		return nil, err
	}

	publicPort, err := strconv.ParseInt(form.Get("public-port"), 10, 32)
	if err != nil {
		return nil, err
	}

	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	networking, err := openapi.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	resp, err := networking.AddPortForwardWithResponse(ctx, openapi.AddPortForward{
		Host:       form.Get("host"),
		HostPort:   int(internalPort),
		PublicIp:   form.Get("public-ip"),
		PublicPort: int(publicPort),
	})
	if err != nil {
		log.Error("error adding port forward: ", err)
		if resp != nil {
			log.Debug("unparsable response: ", resp.Body)
		}
		return nil, err
	}

	if resp.HTTPResponse.StatusCode != http.StatusCreated {
		return nil, plugin.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body)),
		}

	}

	if form.Get("instance") != "" {
		return plugin.Redirect(fmt.Sprintf("/instance/%s", form.Get("instance")))
	}

	return plugin.Redirect(fmt.Sprintf("/networking/public/%s/%s", form.Get("subnet"), form.Get("public-ip")))
}
