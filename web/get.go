// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/networking/web/openapi"
	"entanglement.garden/networking/web/templates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
)

func GetPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	networking, err := openapi.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	network, err := networking.GetNetworkWithResponse(ctx, request.PathParams["id"])
	if err != nil {
		log.Error("error fetching network: ", err)
		return nil, err
	}

	if network.JSON200 == nil {
		return nil, &plugin.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", network.HTTPResponse.Status, " from server: ", string(network.Body)),
		}
	}

	return plugin.WritePage(ctx, request, &templates.GetNetwork{Network: *network.JSON200})
}
