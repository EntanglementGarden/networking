// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/networking/web/openapi"
	"entanglement.garden/networking/web/templates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
)

func ListPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	networking, err := openapi.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	networks, err := networking.ListAllNetworksWithResponse(ctx)
	if err != nil {
		log.Error("error fetching instance list: ", err)
		return nil, err
	}

	if networks.JSON200 == nil {
		log.Error("unexpected ", networks.HTTPResponse.Status, " from server: ", string(networks.Body))
		return nil, err
	}

	return plugin.WritePage(ctx, request, &templates.NetworkList{Networks: *networks.JSON200})
}
