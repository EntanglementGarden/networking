module entanglement.garden/networking/web

go 1.19

require (
	entanglement.garden/api-client v0.0.0-20231130195305-6be5db796d49
	entanglement.garden/webui-plugin v0.0.0-20231201191652-41a8cc57dc2b
	github.com/c-robinson/iplib v1.0.6
	github.com/deepmap/oapi-codegen v1.12.4
	github.com/sirupsen/logrus v1.9.3
	github.com/valyala/quicktemplate v1.7.0
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/uuid v1.3.1 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	golang.org/x/net v0.14.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto v0.0.0-20230306155012-7f2fa6fef1f4 // indirect
	google.golang.org/grpc v1.55.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
