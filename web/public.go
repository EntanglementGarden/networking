// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"net/url"

	"github.com/c-robinson/iplib"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/api-client/egrhyzome"
	"entanglement.garden/networking/web/openapi"
	"entanglement.garden/networking/web/templates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
)

func PublicSubnetsList(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	networking, err := openapi.NewClient(egClient)
	if err != nil {
		return nil, err
	}

	resp, err := networking.ListPublicSubnetsWithResponse(ctx)
	if err != nil {
		log.Error("error fetching instance list: ", err)
		return nil, err
	}

	if resp.JSON200 == nil {
		return nil, &plugin.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body)),
		}
	}

	return plugin.WritePage(ctx, request, &templates.PublicSubnetList{Subnets: *resp.JSON200})
}

func PublicIPsList(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	networking, err := openapi.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	subnetInfo, err := networking.GetPublicSubnetWithResponse(ctx, request.PathParams["subnet"])
	if err != nil {
		log.Error("error fetching adding subnet: ", err)
		return nil, err
	}

	if subnetInfo.JSON200 == nil {
		return nil, &plugin.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", subnetInfo.HTTPResponse.Status, " from server: ", string(subnetInfo.Body)),
		}
	}

	routers, err := networking.GetRoutersWithResponse(ctx)
	if err != nil {
		log.Error("error fetching adding subnet: ", err)
		return nil, err
	}

	if routers.JSON200 == nil {
		return nil, &plugin.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", routers.HTTPResponse.Status, " from server: ", string(routers.Body)),
		}
	}

	iplist, err := networking.GetPublicIpsWithResponse(ctx, request.PathParams["subnet"])
	if err != nil {
		log.Error("error fetching adding subnet: ", err)
		return nil, err
	}

	if iplist.JSON200 == nil {
		return nil, &plugin.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", iplist.HTTPResponse.Status, " from server: ", string(iplist.Body)),
		}
	}

	freeAddress := ""

	_, subnet, err := iplib.ParseCIDR(subnetInfo.JSON200.Subnet)
	if err != nil {
		return nil, err
	}

	existing := map[string]bool{}
	for _, address := range *iplist.JSON200 {
		existing[address.Address] = true
	}

	candidate := subnet.IP()
	for {
		switch s := subnet.(type) {
		case iplib.Net4:
			candidate, err = s.NextIP(candidate)
		default:
			return nil, &plugin.Error{ErrTitle: "Unrecognized IP format"}
		}

		if err == iplib.ErrAddressOutOfRange {
			break
		} else if err != nil {
			return nil, &plugin.Error{
				ErrTitle: "error finding free IP",
				Err:      err,
			}
		}

		if existing[candidate.String()] {
			continue
		}

		freeAddress = candidate.String()
		break
	}

	return plugin.WritePage(ctx, request, &templates.PublicIPList{
		Subnet:      subnetInfo.JSON200.Subnet,
		FreeAddress: freeAddress,
		PublicIPs:   *iplist.JSON200,
		Routers:     *routers.JSON200,
	})
}

func PublicIPCreate(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	form, err := url.ParseQuery(request.Body)
	if err != nil {
		log.Debug("error parsing request form: ", err)
		return nil, err
	}

	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	networking, err := openapi.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	address := form.Get("address")

	resp, err := networking.AddPublicIpWithResponse(ctx, request.PathParams["subnet"], openapi.AddPublicIPBody{
		Address: address,
		Router:  form.Get("router"),
	})
	if err != nil {
		return nil, err
	}

	if resp.JSON201 == nil {
		return nil, &plugin.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body)),
		}
	}

	return plugin.Redirect(fmt.Sprintf("/networking/public/%s/%s", request.PathParams["subnet"], address))
}

func PublicSubnetCreate(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	if request.Method == http.MethodGet {
		return plugin.WritePage(ctx, request, &templates.PublicSubnetCreate{})
	}

	form, err := url.ParseQuery(request.Body)
	if err != nil {
		log.Debug("error parsing request form: ", err)
		return nil, err
	}

	subnet := form.Get("subnet")
	if _, _, err := net.ParseCIDR(subnet); err != nil {
		return nil, &plugin.Error{
			ErrTitle: "invalid subnet",
			Err:      err,
		}
	}

	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	networking, err := openapi.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	resp, err := networking.AddPublicSubnetWithResponse(ctx, openapi.AddPublicSubnetBody{
		Subnet: subnet,
	})
	if err != nil {
		log.Error("error fetching adding subnet: ", err)
		return nil, err
	}

	if resp.JSON201 == nil {
		return nil, &plugin.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body)),
		}
	}

	return plugin.Redirect(fmt.Sprintf("/networking/public/%s", resp.JSON201.Id))
}

func PublicIPPage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		return nil, err
	}

	networking, err := openapi.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	ip := request.PathParams["address"]
	resp, err := networking.GetPortForwardsWithResponse(ctx, &openapi.GetPortForwardsParams{Ip: &ip})
	if err != nil {
		log.Error("error getting port forward info: ", err)
		return nil, err
	}

	if resp.JSON200 == nil {
		return nil, &plugin.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body)),
		}
	}

	rhyzome, err := egrhyzome.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	instances, err := rhyzome.ListAllInstancesWithResponse(ctx)
	if err != nil {
		log.Error("error getting host list: ", err)
		return nil, err
	}

	if instances.JSON200 == nil {
		return nil, &plugin.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", instances.HTTPResponse.Status, " from server: ", string(instances.Body)),
		}
	}

	log.Debug("found ", len(*instances.JSON200), " instances and ", len(*resp.JSON200), " port forwards")

	return plugin.WritePage(ctx, request, &templates.PublicIPPage{
		Address:      ip,
		Subnet:       request.PathParams["subnet"],
		PortForwards: *resp.JSON200,
		AllInstances: *instances.JSON200,
	})
}
