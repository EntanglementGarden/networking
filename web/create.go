// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/networking/web/openapi"
	"entanglement.garden/networking/web/templates"
	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
)

func CreatePage(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	if request.Method == http.MethodGet {
		return plugin.WritePage(ctx, request, &templates.NetworkCreate{})
	}

	form, err := url.ParseQuery(request.Body)
	if err != nil {
		return nil, err
	}

	egClient, err := plugin.GetAPIClient(ctx)
	if err != nil {
		log.Warn("error preparing client connection: ", err)
		return nil, err
	}

	networking, err := openapi.NewClient(egClient)
	if err != nil {
		log.Warn("error preparing networking client connection: ", err)
		return nil, err
	}

	var size *int
	if s := form.Get("size"); s != "" {
		s64, err := strconv.ParseInt(s, 10, 32)
		s := int(s64)

		if err != nil {
			return nil, &plugin.Error{Err: err, ErrTitle: "invalid subnet size"}
		}
		size = &s
	}

	resp, err := networking.CreateNetworkWithResponse(ctx, openapi.CreateNetworkBody{SubnetSize: size})
	if err != nil {
		log.Error("error creating network: ", err)
		return nil, err
	}

	if resp.JSON201 == nil {
		log.Error("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body))
		return nil, &plugin.Error{
			ErrTitle:    "invalid response from server",
			Description: fmt.Sprint("unexpected ", resp.HTTPResponse.Status, " from server: ", string(resp.Body)),
		}
	}

	return plugin.Redirect("/networking/networks/" + resp.JSON201.Id)
}
