# golang package

This is a slightly-modified copy of the openwrt golang package that adds support for building go
modules that aren't in the root of a repo by setting `GO_PKG_BUILD_SUBDIR` to the subdir that the
module lives in.
