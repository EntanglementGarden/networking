// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"database/sql"
	"errors"
	"fmt"
	"math/rand"
	"net"
	"net/http"

	"github.com/c-robinson/iplib"
	echo "github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/api-client/egapi"
	"entanglement.garden/api-client/egrhyzome"
	"entanglement.garden/common/httpx"
	"entanglement.garden/common/identifiers"
	"entanglement.garden/networking/protos"
	"entanglement.garden/networking/server/config"
	"entanglement.garden/networking/server/db"
	"entanglement.garden/networking/server/grpcserver"
	"entanglement.garden/networking/server/networks"
	"entanglement.garden/networking/server/openapi"
)

var defaultSubnetSize = 24

func (server) CreateNetwork(c echo.Context) error {
	ctx := c.Request().Context()

	var req openapi.CreateNetworkBody
	if err := c.Bind(&req); err != nil {
		return err
	}

	if req.SubnetSize == nil {
		req.SubnetSize = &defaultSubnetSize
	}

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	routers, err := queries.GetAllRouters(ctx)
	if err != nil {
		return err
	}

	if len(routers) == 0 {
		return errors.New("initial router not created, please wait until the system is fully setup and try again")
	}

	router := routers[rand.Intn(len(routers))]

	// now that our new network is assigned a router, we much select a subnet for it from the configured InternalIPRanges
	// most of the rest of this overgrown function does that in such a way that it probably wont conflict with another subnet
	// we should improve this or outsource it to netbox

	_, parsedSupernet, err := net.ParseCIDR(config.C.InternalIPRanges[rand.Intn(len(config.C.InternalIPRanges))])
	if err != nil {
		return err
	}

	maxSize, _ := parsedSupernet.Mask.Size()
	if *req.SubnetSize < maxSize {
		log.Debug("requested size: /", *req.SubnetSize, " max size: /", maxSize)
		return errors.New("requested subnet size exceeds configured IP space")
	}

	supernet := iplib.Net4{IPNet: *parsedSupernet}
	subnets, err := supernet.Subnet(*req.SubnetSize)
	if err != nil {
		return fmt.Errorf("error generating subnet: %v", err)
	}

	networks, err := queries.GetNetworksByRouter(ctx, router.ID)
	if err != nil {
		return err
	}

	id := identifiers.GenerateID(identifiers.PrefixNetwork)
	var network db.Network
	for _, subnet := range subnets {

		skipSubnet := false

		for _, existing := range networks {
			_, existingSubnet, err := net.ParseCIDR(existing.Subnet)
			if err != nil {
				log.Warn("invalid CIDR ", existing.Subnet, " in existing subnet ", existing.ID)
				continue
			}

			if existingSubnet.Contains(subnet.IP()) || subnet.Contains(existingSubnet.IP) {
				skipSubnet = true
				break
			}
		}

		if skipSubnet {
			continue
		}

		subnet.IPNet.IP = subnet.FirstAddress() // the IP in the CIDR will be used as the router's address, so take the first address

		network, err = queries.CreateNetwork(ctx, db.CreateNetworkParams{
			ID:        id,
			Router:    router.ID,
			Tenant:    httpx.GetTenant(c),
			Subnet:    subnet.String(),
			EgressIps: db.NullableString(req.EgressIp),
		})
		if err != nil {
			return fmt.Errorf("error storing network in database: %v", err)
		}

		log.Debug("created new subnet ", id, " ", subnet.String())
		break
	}

	egClient, err := egapi.NewInternalClientFromRequest(c.Request())
	if err != nil {
		return err
	}

	rhyzome, err := egrhyzome.NewClient(egClient)
	if err != nil {
		return err
	}

	resp, err := rhyzome.TrunkNicAddNetworkWithResponse(ctx, router.Instance, router.TrunkNic, network.ID)
	if err != nil {
		return err
	}

	if resp.StatusCode() != http.StatusNoContent {
		return fmt.Errorf("error adding vlan to router trunk port: %s %s", resp.Status(), resp.Body)
	}

	grpcserver.PushEvents(router.ID, &protos.Event{
		Network: network.GetProto(),
	})

	vlan := int(network.Vlan)
	return c.JSON(http.StatusCreated, openapi.Network{
		Id:     id,
		Subnet: network.Subnet,
		Vlan:   &vlan,
	})
}

func (server) ListAllNetworks(c echo.Context) error {
	ctx := c.Request().Context()

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	networks, err := queries.GetNetworks(ctx, httpx.GetTenant(c))
	if err != nil {
		return fmt.Errorf("error querying network list: %v", err)
	}

	response := []openapi.Network{}

	for _, n := range networks {
		vlan := int(n.Vlan)
		response = append(response, openapi.Network{
			Id:     n.ID,
			Subnet: n.Subnet,
			Vlan:   &vlan,
		})
	}

	return c.JSON(http.StatusOK, response)
}

func (server) GetNetwork(c echo.Context, id string) error {
	ctx := c.Request().Context()

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	network, err := queries.GetNetwork(ctx, id)
	if err != nil {
		if err == sql.ErrNoRows {
			if id == networks.SystemNetworkID {
				vlan := networks.SystemNetworkVlan
				subnet, _, err := networks.GuessSystemNetwork()
				if err != nil {
					return err
				}

				return c.JSON(http.StatusOK, openapi.Network{
					Id:     networks.SystemNetworkID,
					Subnet: subnet,
					Vlan:   &vlan,
				})
			}
			return c.JSON(http.StatusNotFound, map[string]string{"error": "network not found"})
		}
		return fmt.Errorf("error querying network: %v", err)
	}

	vlan := int(network.Vlan)
	return c.JSON(http.StatusOK, openapi.Network{
		Id:     network.ID,
		Subnet: network.Subnet,
		Vlan:   &vlan,
	})
}
