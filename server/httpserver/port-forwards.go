// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"database/sql"
	"errors"
	"fmt"
	"net/http"

	"entanglement.garden/common/identifiers"
	"entanglement.garden/networking/protos"
	"entanglement.garden/networking/server/db"
	"entanglement.garden/networking/server/grpcserver"
	"entanglement.garden/networking/server/openapi"
	echo "github.com/labstack/echo/v4"
)

func (server) AddPortForward(c echo.Context) error {
	var body openapi.AddPortForward
	if err := c.Bind(&body); err != nil {
		return err
	}

	if body.HostPort < 1 || body.HostPort > 65535 {
		return errors.New("invalid host port")
	}

	if body.PublicPort < 1 || body.PublicPort > 65535 {
		return errors.New("invalid public port")
	}

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	ctx := c.Request().Context()
	ip, err := queries.GetPublicIPByIP(ctx, body.PublicIp)
	if err != nil {
		if err == sql.ErrNoRows {
			return c.JSON(404, map[string]string{"error": "ip not found"})
		}
		return fmt.Errorf("error looking up public IP: %v", err)
	}

	portForward, err := queries.AddPortForward(ctx, db.AddPortForwardParams{
		ID:           identifiers.GenerateID(identifiers.PrefixPortForward),
		PublicIp:     ip.ID,
		PublicPort:   int64(body.PublicPort),
		InternalHost: body.Host,
		InternalPort: int64(body.HostPort),
	})
	if err != nil {
		return err
	}

	router, err := queries.GetRouterForPortForward(ctx, portForward.ID)
	if err != nil {
		return err
	}

	p, err := portForward.GetProto(ctx, queries)
	if err != nil {
		return err
	}

	grpcserver.PushEvents(router, &protos.Event{PortForward: p})

	oapi, err := portForward.GetOpenAPI(ctx, queries)
	if err != nil {
		return fmt.Errorf("error getting response data: %v", err)
	}
	return c.JSON(http.StatusCreated, oapi)
}

func (server) DeletePortForward(c echo.Context, id string) error {
	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	err = queries.DeletePortForward(c.Request().Context(), id)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

func (server) GetPortForwards(c echo.Context, params openapi.GetPortForwardsParams) error {
	ctx := c.Request().Context()

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	var portForwards []db.PortForward
	if params.Host != nil {
		portForwards, err = queries.GetPortForwardsByInternalHost(ctx, *params.Host)
	} else if params.Ip != nil {
		portForwards, err = queries.GetPortForwardsByIP(ctx, *params.Ip)
	} else {
		portForwards, err = queries.GetPortForwards(ctx)
	}
	if err != nil {
		return err
	}

	resp := []openapi.PortForward{}
	for _, portForward := range portForwards {
		pf, err := portForward.GetOpenAPI(ctx, queries)
		if err != nil {
			return fmt.Errorf("error preparing port-forward response: %v", err)
		}
		resp = append(resp, pf)
	}

	return c.JSON(http.StatusOK, resp)
}
