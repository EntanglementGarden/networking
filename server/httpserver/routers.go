// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	echo "github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/api-client/egapi"
	"entanglement.garden/api-client/egrhyzome"
	"entanglement.garden/common/httpx"
	"entanglement.garden/common/identifiers"
	"entanglement.garden/networking/server/config"
	"entanglement.garden/networking/server/db"
	"entanglement.garden/networking/server/openapi"
	"entanglement.garden/networking/server/pki"
)

func (server) AddRouter(c echo.Context) error {
	ctx := c.Request().Context()

	egClient, err := egapi.NewInternalClientFromRequest(c.Request())
	if err != nil {
		return err
	}

	rhyzome, err := egrhyzome.NewClient(egClient)
	if err != nil {
		return err
	}

	instance, err := rhyzome.CreateInstanceWithResponse(ctx, egrhyzome.CreateInstanceRequestBody{
		BaseImage:      config.C.RouterBaseImage,
		BaseImageQcow2: config.C.RouterBaseImageQcow2,
		Cpu:            1,
		DiskSize:       10,
		Memory:         1024,
	})
	if err != nil {
		return fmt.Errorf("error creating instance: %v", err)
	}

	if instance.JSON201 == nil {
		return fmt.Errorf("unexpected response from rhyzome-libvirt: %s %s", instance.Status(), string(instance.Body))
	}

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	trunkNic := ""
	for _, nic := range instance.JSON201.Interfaces {
		if nic.Trunk != nil && *nic.Trunk {
			trunkNic = nic.Id
			break
		}
	}

	if trunkNic == "" {
		return errors.New("no trunk nic found in response from instance service when trying to create a router")
	}

	var body openapi.AddRouterBody
	if err := c.Bind(&body); err != nil {
		return err
	}

	router, err := queries.AddRouter(ctx, db.AddRouterParams{
		ID:            identifiers.GenerateID(identifiers.PrefixRouter),
		Instance:      instance.JSON201.ResourceId,
		TrunkNic:      trunkNic,
		PublicGateway: body.PublicGateway,
	})
	if err != nil {
		return err
	}

	return c.JSON(http.StatusCreated, router.GetOpenAPI())
}

func (server) GetRouters(c echo.Context) error {
	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	dbRouters, err := queries.GetAllRouters(c.Request().Context())
	if err != nil {
		return err
	}

	routers := []openapi.Router{}

	for _, r := range dbRouters {
		routers = append(routers, r.GetOpenAPI())
	}

	return c.JSON(http.StatusOK, routers)
}

func (server) GenerateRouterBootData(c echo.Context) error {
	requestToken, err := httpx.GetJWT(c.Request().Context())
	if err != nil {
		return err
	}

	log.WithField("token", requestToken).Debug("generating PKI token for what we hope is a new router, requesting token (should be our own system token)")

	var body openapi.RouterBootDataRequest
	err = json.NewDecoder(c.Request().Body).Decode(&body)
	if err != nil {
		return fmt.Errorf("unparsable request body: %v", err)
	}

	biosdata := url.Values{}
	biosdata.Set("s", config.InstallData.SystemInstanceBootstrapIP) // TODO: HypervisorCallbackURL is the hypervisor's IP but we need to set this value to the system VM's IP

	token, err := pki.GetToken(body.Hostname)
	if err != nil {
		return fmt.Errorf("error issuing token: %v", err)
	}
	biosdata.Set("t", token)

	fp, err := pki.GetFingerprint()
	if err != nil {
		return fmt.Errorf("error getting CA fingerprint: %v", err)
	}
	biosdata.Set("f", fp)

	return c.JSON(http.StatusCreated, openapi.NewRouterBootData{
		Biosdata: biosdata.Encode(),
	})
}
