// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"context"
	_ "embed"
	"net/http"

	echo "github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"

	"entanglement.garden/common/httpx"
	"entanglement.garden/networking/server/config"
	"entanglement.garden/networking/server/openapi"
)

type server struct{}

var (
	//go:embed web.ewp
	ewp []byte

	e *echo.Echo
)

func Serve() {
	e = httpx.NewServerWithEWP(ewp)
	openapi.RegisterHandlersWithBaseURL(e, &server{}, "/entanglement.garden/networking")

	e.GET("/.well-known/rhyzome/ready", func(c echo.Context) error { return c.String(http.StatusOK, "ready") })

	log.Info("starting http listener on ", config.C.HTTPBind)

	err := e.Start(config.C.HTTPBind)
	if err != http.ErrServerClosed {
		log.Fatal(err)
	}
}

func Shutdown() error {
	return e.Shutdown(context.Background())
}
