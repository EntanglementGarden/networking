// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"database/sql"
	"encoding/base64"
	"fmt"
	"net/http"

	echo "github.com/labstack/echo/v4"

	"entanglement.garden/common/httpx"
	"entanglement.garden/networking/protos"
	"entanglement.garden/networking/server/db"
	"entanglement.garden/networking/server/grpcserver"
	"entanglement.garden/networking/server/networks"
	"entanglement.garden/networking/server/openapi"
)

func (server) GetHostsByNetwork(c echo.Context, id string) error {
	ctx := c.Request().Context()

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	hosts, err := queries.GetHostsByNetwork(ctx, id)
	if err != nil && err != sql.ErrNoRows {
		return fmt.Errorf("error querying network hosts: %v", err)
	}

	response := []openapi.Host{}
	for _, h := range hosts {
		response = append(response, openapi.Host{
			Addresses: []string{h.Ipv4},
			Id:        h.ID,
			Instance:  h.Instance,
			Mac:       h.Mac,
			Network:   h.Network,
		})
	}

	return c.JSON(http.StatusOK, response)
}

func (server) AddHost(c echo.Context, networkID string) error {
	ctx := c.Request().Context()

	var req openapi.AddHostBody
	if err := c.Bind(&req); err != nil {
		return err
	}

	userData, err := base64.StdEncoding.DecodeString(req.UserData)
	if err != nil {
		return err
	}

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	network, err := queries.GetNetwork(ctx, networkID)
	if err != nil {
		if err == sql.ErrNoRows {
			if networkID == networks.SystemNetworkID {
				// edge case for initial router creation, this endpoint gets called for the system router before the router is in the database
				return c.JSON(http.StatusCreated, openapi.NewHostDetails{
					Vlan: networks.SystemNetworkVlan,
				})
			}
			return c.JSON(http.StatusNotFound, map[string]string{"error": "network not found"})
		}
		return fmt.Errorf("error getting network: %v", err)
	}

	host, err := queries.CreateHostGenerateIP(ctx, network.Subnet, db.CreateHostParams{
		ID:       req.Id,
		Tenant:   httpx.GetTenant(c),
		Instance: req.Instance,
		Mac:      req.Mac,
		Network:  networkID,
		UserData: userData,
	})
	if err != nil {
		return err
	}

	grpcserver.PushEvents(network.Router, &protos.Event{Host: host.GetProto()})

	return c.JSON(http.StatusCreated, openapi.NewHostDetails{
		Addresses: []string{host.Ipv4},
		Vlan:      int(network.Vlan),
	})
}
