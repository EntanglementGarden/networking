// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package httpserver

import (
	"errors"
	"fmt"
	"net"
	"net/http"

	"github.com/c-robinson/iplib"
	echo "github.com/labstack/echo/v4"
	sqlite3 "github.com/mattn/go-sqlite3"

	"entanglement.garden/common/identifiers"
	"entanglement.garden/networking/protos"
	"entanglement.garden/networking/server/db"
	"entanglement.garden/networking/server/grpcserver"
	"entanglement.garden/networking/server/openapi"
)

func (server) AddPublicSubnet(c echo.Context) error {
	var body openapi.AddPublicSubnetBody
	if err := c.Bind(&body); err != nil {
		return err
	}

	_, cidr, err := net.ParseCIDR(body.Subnet)
	if err != nil {
		return fmt.Errorf("unable to parse CIDR in request: %v", err)
	}

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	publicSubnet, err := queries.CreatePublicSubnet(c.Request().Context(), db.CreatePublicSubnetParams{
		ID:     identifiers.GenerateID(identifiers.PrefixPublicSubnet),
		Subnet: cidr.String(),
	})
	if err != nil {
		return err
	}

	return c.JSON(http.StatusCreated, publicSubnet.GetOpenAPI())
}

func (server) ListPublicSubnets(c echo.Context) error {
	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	subnets, err := queries.GetPublicSubnets(c.Request().Context())
	if err != nil {
		return err
	}

	resp := []openapi.PublicSubnet{}
	for _, subnet := range subnets {
		resp = append(resp, subnet.GetOpenAPI())
	}

	return c.JSON(http.StatusOK, resp)
}

func (server) GetPublicSubnet(c echo.Context, id string) error {
	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	subnet, err := queries.GetPublicSubnet(c.Request().Context(), id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, subnet.GetOpenAPI())
}

func (server) AddPublicIp(c echo.Context, subnet string) error {
	var body openapi.AddPublicIPBody
	if err := c.Bind(&body); err != nil {
		return err
	}

	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	ctx := c.Request().Context()

	dbSubnet, err := queries.GetPublicSubnet(ctx, subnet)
	if err != nil {
		return err
	}

	ip := net.ParseIP(body.Address)
	_, s, err := iplib.ParseCIDR(dbSubnet.Subnet)
	if err != nil {
		return err
	}

	if !s.Contains(net.ParseIP(body.Address)) {
		return fmt.Errorf("requested IP %s is not in subnet %s", body.Address, dbSubnet.Subnet)
	}

	publicIp, err := queries.AddPublicIP(ctx, db.AddPublicIPParams{
		ID:     identifiers.GenerateID(identifiers.PrefixPublicIP),
		Ip:     ip.String(),
		Subnet: subnet,
		Router: body.Router,
	})
	if err != nil {
		if sqliteErr, ok := err.(sqlite3.Error); ok {
			if sqliteErr.Code == sqlite3.ErrConstraint {
				// this is a shitty way to detect which constraint was violated but I don't see a better way to do it
				// need to check which constraint is violated because this is also the first time the mac
				// is validated as being unique, and non-unique MACs need to return err, not continue
				if sqliteErr.Error() == "UNIQUE constraint failed: public_ip.unique_ipv4_per_network" {
					return errors.New("requested address already allocated")
				}
			}
		}
		return err
	}

	p, err := publicIp.GetProto(ctx, queries)
	if err != nil {
		return err
	}

	grpcserver.PushEvents(publicIp.Router, &protos.Event{PublicIp: p})

	return c.JSON(http.StatusCreated, publicIp.GetOpenAPI())
}

func (server) GetPublicIps(c echo.Context, subnet string) error {
	queries, dbc, err := db.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()

	ips, err := queries.GetPublicIPsBySubnet(c.Request().Context(), subnet)
	if err != nil {
		return err
	}

	resp := []openapi.PublicIP{}
	for _, ip := range ips {
		resp = append(resp, ip.GetOpenAPI())
	}

	return c.JSON(http.StatusOK, resp)
}
