// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package networks

import (
	"fmt"
	"net"

	"github.com/c-robinson/iplib"

	"entanglement.garden/networking/server/config"
)

const (
	SystemNetworkID   = "system"
	SystemNetworkVlan = 10
)

func GuessSystemNetwork() (string, string, error) {
	_, supernetCIDR, err := net.ParseCIDR(config.C.InternalIPRanges[0])
	if err != nil {
		return "", "", fmt.Errorf("error parsing configured internal IP range %s: %v", config.C.InternalIPRanges[0], err)
	}

	supernet := iplib.Net4{IPNet: *supernetCIDR}

	subnets, err := supernet.Subnet(24)
	if err != nil {
		return "", "", fmt.Errorf("error selecting a /24 from the configured IP range %s: %v", config.C.InternalIPRanges[0], err)
	}

	if len(subnets) == 0 {
		return "", "", fmt.Errorf("no /24s available in configured IP range %s", config.C.InternalIPRanges[0])
	}

	subnet := subnets[0]
	subnet.IPNet.IP = subnet.FirstAddress()

	return subnet.String(), subnet.IP().String(), nil
}
