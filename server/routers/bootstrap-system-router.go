// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package routers

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"entanglement.garden/api-client/egapi"
	"entanglement.garden/api-client/egrhyzome"
	"entanglement.garden/common/dbx"
	"entanglement.garden/common/identifiers"
	"entanglement.garden/common/logging"
	"entanglement.garden/networking/server/config"
	"entanglement.garden/networking/server/db"
	"entanglement.garden/networking/server/networks"
)

const (
	SystemRouterHostname = "system-router"
)

func EnsureSystemRouter(ctx context.Context) error {
	log := logging.GetLog(ctx)
	log.Debug("making sure the system router exists")

	dbc, err := dbx.Get()
	if err != nil {
		return err
	}
	defer dbc.Close()
	queries := db.New(dbc)

	routers, err := queries.GetAllRouters(ctx)
	if err != nil {
		return err
	}

	if len(routers) > 0 {
		log.Debug("system router exists 👍")
		return nil
	}

	log.Info("creating system router VM")

	egClient, err := egapi.NewInternalClient()
	if err != nil {
		return err
	}

	rhyzome, err := egrhyzome.NewClient(egClient)
	if err != nil {
		return err
	}

	netctx, cancel := context.WithTimeout(ctx, time.Minute)
	defer cancel()

	log.Debug("asking rhyzome to create instance")
	instance, err := rhyzome.CreateInstanceWithResponse(netctx, egrhyzome.CreateInstanceRequestBody{
		BaseImage:      config.C.RouterBaseImage,
		BaseImageQcow2: config.C.RouterBaseImageQcow2,
		Cpu:            1,
		DiskSize:       10,
		Memory:         1024,
		SpecialCases:   &[]string{"ROUTER", "BOOTSTRAP_NETWORK"},
		Networks:       []string{"system"},
	})
	if err != nil {
		return fmt.Errorf("error creating instance: %v", err)
	}

	if instance.JSON201 == nil {
		return fmt.Errorf("unexpected response from rhyzome: %s %s", instance.Status(), string(instance.Body))
	}

	log.Debug("instance created")

	trunkNic := ""
	for _, nic := range instance.JSON201.Interfaces {
		if nic.Trunk != nil && *nic.Trunk {
			trunkNic = nic.Id
			break
		}
	}

	if trunkNic == "" {
		return errors.New("no trunk nic found in response from instance service when trying to create a router")
	}

	log.Info("adding configuration for system router to database")

	log.Debug("adding router to db")
	router, err := queries.AddRouter(ctx, db.AddRouterParams{
		ID:            identifiers.GenerateID(identifiers.PrefixRouter),
		Instance:      instance.JSON201.ResourceId,
		TrunkNic:      trunkNic,
		PublicGateway: config.InstallData.NetworkPublicGateway,
	})
	if err != nil {
		return fmt.Errorf("failed to add system router to database: %v", err)
	}

	subnet, routerIP, err := networks.GuessSystemNetwork()
	if err != nil {
		return fmt.Errorf("failed to guess system network: %v", err)
	}

	publicSubnet := identifiers.GenerateID(identifiers.PrefixPublicSubnet)
	log.WithField("subnet_id", publicSubnet).Debug("adding public subnet")
	_, err = queries.CreatePublicSubnet(ctx, db.CreatePublicSubnetParams{
		ID:     publicSubnet,
		Subnet: config.InstallData.NetworkPublicSubnet,
	})
	if err != nil {
		return fmt.Errorf("error adding public subnet to database: %v", err)
	}

	publicIP := identifiers.GenerateID(identifiers.PrefixPublicIP)
	log.WithField("public_ip", publicIP).Debug("adding public ip")
	_, err = queries.AddPublicIP(ctx, db.AddPublicIPParams{
		ID:     publicIP,
		Ip:     config.InstallData.NetworkPublicIP,
		Subnet: publicSubnet,
		Router: router.ID,
	})
	if err != nil {
		return fmt.Errorf("error adding public ip to database: %v", err)
	}

	log.WithField("subnet", subnet).Debug("creating system network")
	_, err = queries.CreateSystemNetwork(ctx, db.CreateSystemNetworkParams{
		ID:        networks.SystemNetworkID,
		Router:    router.ID,
		Tenant:    identifiers.DefaultTenant,
		Subnet:    subnet,
		EgressIps: db.NullableString(publicIP),
	})
	if err != nil {
		return fmt.Errorf("error adding system network to database: %v", err)
	}

	log.WithField("ip", routerIP).Debug("marking new router as waiting for bootstrap")
	err = queries.SetWaitingForRouterBootstrap(ctx, db.SetWaitingForRouterBootstrapParams{
		ID:              router.ID,
		PostBootstrapIp: sql.NullString{Valid: true, String: routerIP},
	})
	if err != nil {
		return fmt.Errorf("error marking router as waiting for bootstrap in database: %v", err)
	}

	log = log.WithField("system_instance_nic", config.InstallData.SystemInstanceNIC)

	log.Debug("generating IP for system instance")
	_, err = queries.CreateHostGenerateIP(ctx, subnet, db.CreateHostParams{
		ID:       config.InstallData.SystemInstanceNIC,
		Tenant:   identifiers.DefaultTenant,
		Instance: config.InstallData.SystemInstanceID,
		Mac:      config.InstallData.SystemInstanceMAC,
		Network:  networks.SystemNetworkID,
	})
	if err != nil {
		return fmt.Errorf("error adding services VM to database: %v", err)
	}

	log.WithField("first_node_mac", config.InstallData.FirstNodeMAC).Debug("generating IP for first node")
	_, err = queries.CreateHostGenerateIP(ctx, subnet, db.CreateHostParams{
		ID:       config.InstallData.FirstNodeNIC,
		Tenant:   identifiers.DefaultTenant,
		Instance: config.InstallData.FirstNodeID,
		Mac:      config.InstallData.FirstNodeMAC,
		Network:  networks.SystemNetworkID,
	})
	if err != nil {
		return fmt.Errorf("error adding hypervisor to database: %v", err)
	}

	for _, port := range []int64{22, 80, 443, 8443, 9090} {
		log.WithField("port", port).Debug("adding port forward for system instance")
		_, err := queries.AddPortForward(ctx, db.AddPortForwardParams{
			ID:           identifiers.GenerateID(identifiers.PrefixPortForward),
			PublicIp:     publicIP,
			PublicPort:   port,
			InternalHost: config.InstallData.SystemInstanceNIC,
			InternalPort: port,
		})
		if err != nil {
			return fmt.Errorf("error adding port forward: %v", err)
		}
	}

	log.Info("done creating initial router")

	return nil
}
