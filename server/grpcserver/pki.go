// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver

import (
	"context"

	"entanglement.garden/networking/protos"
	"entanglement.garden/networking/server/pki"
)

func (server) GetToken(_ context.Context, instance *protos.Instance) (*protos.Token, error) {
	token, err := pki.GetToken(instance.Hostname)
	if err != nil {
		return nil, err
	}

	return &protos.Token{
		Token: token,
	}, nil
}
