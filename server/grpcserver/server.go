// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver

import (
	"net"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	"entanglement.garden/common/grpcx"
	"entanglement.garden/networking/protos"
	"entanglement.garden/networking/server/config"
)

type server struct {
	protos.UnimplementedOpenWRTServer
}

var grpcServer *grpc.Server

func Listen() {
	grpcServer = grpcx.NewServer(config.C.PKI)

	listener, err := net.Listen("tcp", config.C.GRPCBind)
	if err != nil {
		log.Error("grpc server failed to listen:", err)
		return
	}

	protos.RegisterOpenWRTServer(grpcServer, server{})

	log.Info("Starting gRPC server on ", config.C.GRPCBind)
	err = grpcServer.Serve(listener)
	if err != nil {
		log.Fatal("error from grpc listener: ", err)
	}
}

func Shutdown() {
	grpcServer.Stop()
}
