// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package grpcserver

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"net"
	"sync"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/peer"

	"entanglement.garden/api-client/egapi"
	"entanglement.garden/api-client/egrhyzome"
	"entanglement.garden/common/grpcx"
	"entanglement.garden/networking/protos"
	"entanglement.garden/networking/server/config"
	"entanglement.garden/networking/server/db"
)

var (
	eventsChannels      = make(map[string]chan *protos.Event)
	eventsChannelsMutex = sync.Mutex{}
)

func (server) EventStream(_ *protos.Empty, events protos.OpenWRT_EventStreamServer) error {
	ctx := events.Context()

	peerIdentity, err := grpcx.GetPeerName(ctx)
	if err != nil {
		return err
	}

	logger := log.WithField("node", peerIdentity.CommonName)
	logger.Info("grpc connection from ", peerIdentity)

	queries, dbConn, err := db.Get()
	if err != nil {
		log.Error("error connecting to db: ", err)
		return err
	}
	defer dbConn.Close()

	router, err := queries.GetRouterByInstanceID(ctx, peerIdentity.CommonName)
	if err != nil {
		logger.Error("error getting node from db: ", err)
		return err
	}

	if router.PostBootstrapIp.Valid {
		if err := checkPostBootstrap(ctx, router); err != nil {
			log.Error("error checking if this is a successful post-bootstrap connect: ", err)
		}
	}

	// begin listening for events sent to this router before sending expected state,
	// so updates to the expected state after query and before the state is fully sent
	// don't get missed
	ch, err := addEventChannel(router)
	if err != nil {
		return err
	}
	defer removeEventChannel(router)

	if post_bootstrap_ip, err := ensureExisting(ctx, queries, router, events); err != nil {
		log.Error("error ensuring existing resources: ", err)
		return err
	} else if post_bootstrap_ip && router.PostBootstrapIp.Valid {
		// this condition is true during the bootstrap process after the system network has been sent to the client.
		// it will disconnect the client, which should reconnect over the system network
		log.Info("client just completed bootstrapping. Closing stream so it will reconnect over the system network")
		return nil
	}

	for {
		select {
		case e := <-ch:
			err := events.Send(e)
			if err != nil {
				log.Error("error sending event: ", err)
				return err
			}

		case <-ctx.Done():
			return nil
		}
	}
}

func addEventChannel(node db.Router) (chan *protos.Event, error) {
	eventsChannelsMutex.Lock()
	_, ok := eventsChannels[node.ID]
	if ok {
		log.Error("refusing duplicate connection from ", node.ID)
		eventsChannelsMutex.Unlock()
		return nil, errors.New("duplicate connection from node not allowed")
	}

	ch := make(chan *protos.Event, 100)
	eventsChannels[node.ID] = ch
	eventsChannelsMutex.Unlock()

	return ch, nil
}

func removeEventChannel(node db.Router) {
	log.Debug("lost connection from ", node.ID)
	eventsChannelsMutex.Lock()
	ch, ok := eventsChannels[node.ID]
	if !ok {
		log.Debug("EventStream endpoint closed with no clients for this node")
		eventsChannelsMutex.Unlock()
		return
	}
	close(ch)
	delete(eventsChannels, node.ID)
	eventsChannelsMutex.Unlock()
}

func ensureExisting(ctx context.Context, queries *db.Queries, router db.Router, events protos.OpenWRT_EventStreamServer) (bool, error) {
	networks, err := queries.GetNetworksByRouter(ctx, router.ID)
	if err != nil {
		log.Error("error finding networks for router")
		return false, err
	}

	log.Debug("ensuring state of ", len(networks), " networks")

	servicesVMInternalIP := ""

	for _, network := range networks {
		err = events.Send(&protos.Event{Network: network.GetProto()})
		if err != nil {
			log.Error("error sending event to gRPC client")
			return false, err
		}

		hosts, err := queries.GetHostsByNetwork(ctx, network.ID)
		if err != nil {
			log.Error("error looking up hosts for network")
			return false, err
		}

		log.Debug("ensuring state of ", len(hosts), " hosts for network ", network.ID)
		for _, host := range hosts {
			err = events.Send(&protos.Event{Host: host.GetProto()})
			if err != nil {
				log.Error("error sending gRPC ensure host event to client")
				return false, err
			}

			if host.Instance == config.InstallData.SystemInstanceID {
				servicesVMInternalIP = host.Ipv4
			}
		}
	}

	publicIPs, err := queries.GetPublicIPsByRouter(ctx, router.ID)
	if err != nil {
		log.Error("error getting public IPs for router")
		return false, err
	}

	log.Debug("ensuring state of ", len(publicIPs), " public IPs")
	for _, ip := range publicIPs {
		p, err := ip.GetProto(ctx, queries)
		if err != nil {
			log.Error("error generating proto for public IP")
			return false, err
		}

		err = events.Send(&protos.Event{PublicIp: p})
		if err != nil {
			log.Error("error sending gRPC ensure public IP event")
			return false, err
		}
	}

	portForwards, err := queries.GetPortForwardsByRouter(ctx, router.ID)
	if err != nil {
		log.Error("error looking up port forwards for router")
		return false, err
	}

	log.Debug("ensuring state of ", len(portForwards), " port forwards")
	for _, pf := range portForwards {
		p, err := pf.GetProto(ctx, queries)
		if err != nil {
			log.Error("error generating proto for port forward")
			return false, err
		}

		err = events.Send(&protos.Event{PortForward: p})
		if err != nil {
			log.Error("error sending gRPC ensure port forward event")
			return false, err
		}
	}

	if servicesVMInternalIP != "" {
		log.Info("telling rhyzome that the system network is running")
		egClient, err := egapi.NewInternalClient()
		if err != nil {
			return false, err
		}

		rhyzome, err := egrhyzome.NewClient(egClient)
		if err != nil {
			return false, err
		}

		_, err = rhyzome.BootstrapSystemNetworkReady(ctx, servicesVMInternalIP)
		if err != nil {
			return false, fmt.Errorf("error removing bootstrap network from services VM: %v", err)
		}
	}

	return servicesVMInternalIP != "", nil
}

// PushEvents to a connected router. if relevant router is not connected, messages is silently ignored
func PushEvents(router string, events ...*protos.Event) {
	eventsChannelsMutex.Lock()
	defer eventsChannelsMutex.Unlock()

	ch, ok := eventsChannels[router]
	if !ok {
		return
	}

	for _, event := range events {
		ch <- event
	}
}

func checkPostBootstrap(ctx context.Context, router db.Router) error {
	p, ok := peer.FromContext(ctx)
	if !ok {
		return errors.New("no peer connection information on context")
	}

	host, _, err := net.SplitHostPort(p.Addr.String())
	if err != nil {
		return fmt.Errorf("error parsing connecting IP string (%s): %v", p.Addr.String(), err)
	}

	if host != router.PostBootstrapIp.String {
		log.Info("got a connection from the initial router from an IP other than it's expected system network IP: ", host)
		return nil
	}

	egClient, err := egapi.NewInternalClient()
	if err != nil {
		return err
	}

	rhyzome, err := egrhyzome.NewClient(egClient)
	if err != nil {
		return err
	}

	_, err = rhyzome.BootstrapCompleteWithResponse(ctx)
	if err != nil {
		return err
	}

	queries, dbConn, err := db.Get()
	if err != nil {
		return err
	}
	defer dbConn.Close()

	err = queries.SetWaitingForRouterBootstrap(ctx, db.SetWaitingForRouterBootstrapParams{
		ID:              router.ID,
		PostBootstrapIp: sql.NullString{},
	})
	if err != nil {
		return err
	}

	return nil
}
