// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import (
	"context"
	"encoding/binary"
	"fmt"
	"math"
	"math/rand"
	"net"

	sqlite3 "github.com/mattn/go-sqlite3"

	"entanglement.garden/networking/protos"
	"entanglement.garden/networking/server/config"
)

const (
	systemIPsPerSubnet       = 10 // number of IPs total to reserve for the system
	systemIPsAtStartOfSubnet = 5  // number of reserved IPs that will be at the beginning of the subnet (remaining will be at end)
	maxIPGenerationAttempts  = 10 // try this many times to get a unique IP address, and give up when it fails
)

func (h Host) GetProto() *protos.Host {
	return &protos.Host{
		NetworkId:    h.Network,
		State:        protos.State_ACTIVE,
		Mac:          h.Mac,
		Ip:           h.Ipv4,
		Instance:     h.Instance,
		UserData:     h.UserData,
		ServicesHost: h.Instance == config.InstallData.SystemInstanceID,
	}
}

func (q Queries) CreateHostGenerateIP(ctx context.Context, subnet string, params CreateHostParams) (*Host, error) {
	if len(params.Mac) != 17 {
		return nil, fmt.Errorf("invalid mac while generating host IP: %s", params.Mac)
	}

	var host Host
	for attempts := 0; attempts < maxIPGenerationAttempts; attempts++ {
		ip, err := generateIP(subnet)
		if err != nil {
			return nil, err
		}

		params.Ipv4 = ip.String()

		host, err = q.CreateHost(ctx, params)
		if err != nil {
			if sqliteErr, ok := err.(sqlite3.Error); ok {
				if sqliteErr.Code == sqlite3.ErrConstraint {
					// this is a shitty way to detect which constraint was violated but I don't see a better way to do it
					// need to check which constraint is violated because this is also the first time the mac
					// is validated as being unique, and non-unique MACs need to return err, not continue
					// if the IP is a duplicate, loop again
					if sqliteErr.Error() == "UNIQUE constraint failed: hosts.unique_ipv4_per_network" {
						continue
					}
				}
			}
			return nil, err
		}
		break
	}

	if host.Ipv4 == "" {
		return nil, fmt.Errorf("failed to generate a unique IP address after %d attempts", maxIPGenerationAttempts)
	}

	return &host, nil
}

func generateIP(subnetStr string) (net.IP, error) {
	_, subnet, err := net.ParseCIDR(subnetStr)
	if err != nil {
		return net.IP{}, fmt.Errorf("error parsing network subnet %s: %v", subnetStr, err)
	}

	// find the size of the pool of generatable addresses (subnet size - systemIPsPerSubnet)
	ones, bits := subnet.Mask.Size()
	size := math.Pow(2, float64(bits-ones)) - systemIPsPerSubnet

	// find the first address in the available address pool
	binaryIP := binary.BigEndian.Uint32(subnet.IP) + systemIPsAtStartOfSubnet

	// add a random number between zero and size to the lowest IP
	binaryIP = binaryIP + rand.Uint32()%uint32(size)

	// put result into a net.IP
	ip := make(net.IP, 4)
	binary.BigEndian.PutUint32(ip, binaryIP)

	return ip, nil
}
