// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import (
	"database/sql"
	"embed"

	_ "github.com/mattn/go-sqlite3"

	"entanglement.garden/common/dbx"
)

//go:embed sqlite/migrations
var migrations embed.FS

func Migrate() error {
	return dbx.Migrate(migrations)
}

// Deprecated. Use dbx from common:
//
//	  dbc, err := dbx.Get()
//		 if err != nil {
//		   return err
//		 }
//	  defer dbc.Close()
//	  queries := db.New(dbc)
func Get() (*Queries, *sql.DB, error) {
	db, err := sql.Open("sqlite3", dbx.GetDBPath())
	if err != nil {
		return nil, nil, err
	}

	_, err = db.Exec("PRAGMA foreign_keys = ON")
	if err != nil {
		return nil, nil, err
	}

	return New(db), db, nil
}

// NullableString accepts a string and returns an sql.NullString
// if the input string is zero-length, the output will be marked
// null
func NullableString(s string) sql.NullString {
	return sql.NullString{
		Valid:  s != "",
		String: s,
	}
}
