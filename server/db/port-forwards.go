// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import (
	"context"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/networking/protos"
	"entanglement.garden/networking/server/openapi"
)

func (p PortForward) GetOpenAPI(ctx context.Context, queries *Queries) (openapi.PortForward, error) {
	publicIP, err := queries.GetPublicIPByID(ctx, p.PublicIp)
	if err != nil {
		return openapi.PortForward{}, err
	}
	return openapi.PortForward{
		Id:         p.ID,
		Host:       p.InternalHost,
		HostPort:   int(p.InternalPort),
		PublicIp:   publicIP.Ip,
		PublicPort: int(p.PublicPort),
	}, nil
}

func (p PortForward) GetProto(ctx context.Context, queries *Queries) (*protos.PortForward, error) {
	host, err := queries.GetHost(ctx, p.InternalHost)
	if err != nil {
		log.Error("error getting host ", p.InternalHost, " for port forward")
		return nil, err
	}

	publicIP, err := queries.GetPublicIPByID(ctx, p.PublicIp)
	if err != nil {
		log.Error("error getting public IP ", p.PublicIp, " for port forward")
		return nil, err
	}

	return &protos.PortForward{
		Id:              p.ID,
		PublicIpId:      publicIP.ID,
		PublicIp:        publicIP.Ip,
		PublicPort:      int32(p.PublicPort),
		InternalNetwork: host.Network,
		InternalHost:    host.Ipv4,
		InternalPort:    int32(p.InternalPort),
	}, nil
}
