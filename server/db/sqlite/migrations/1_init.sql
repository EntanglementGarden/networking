-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- +goose Up
-- +goose StatementBegin
CREATE TABLE routers (
    id TEXT UNIQUE NOT NULL,
    instance TEXT UNIQUE NOT NULL,
    trunk_nic TEXT UNIQUE NOT NULL,
    public_gateway TEXT UNIQUE NOT NULL,
    post_bootstrap_ip TEXT
);

CREATE TABLE networks (
    id TEXT UNIQUE NOT NULL,
    router TEXT NOT NULL REFERENCES routers(id),
    tenant TEXT NOT NULL,
    vlan INT UNIQUE NOT NULL, -- TODO: think about rules (eg. less than 4096)
    subnet TEXT NOT NULL, -- TODO: prevent overlaps at the database level (might be hard in sqlite)
    egress_ips TEXT REFERENCES public_ips(id)
);

CREATE TABLE hosts (
    id TEXT UNIQUE NOT NULL,
    tenant TEXT NOT NULL,
    instance TEXT NOT NULL,
    mac TEXT UNIQUE NOT NULL,
    network TEXT REFERENCES networks(id) NOT NULL,
    ipv4 TEXT NOT NULL,
    trunk BOOLEAN NOT NULL DEFAULT false,
    user_data BLOB,

    CONSTRAINT unique_ipv4_per_network UNIQUE(ipv4, network)
);

CREATE TABLE public_subnets (
    id TEXT UNIQUE NOT NULL,
    subnet TEXT UNIQUE NOT NULL
);

CREATE TABLE public_ips (
    id TEXT UNIQUE NOT NULL, -- this is not exposed to the API, only used to as a UCI section name (at time of writing)
    ip TEXT UNIQUE NOT NULL,
    subnet TEXT NOT NULL REFERENCES public_subnets(id),
    router TEXT NOT NULL REFERENCES routers(id)
);

CREATE TABLE port_forwards (
    id TEXT UNIQUE NOT NULL,
    public_ip TEXT NOT NULL REFERENCES public_ips(id),
    public_port INT NOT NULL,
    internal_host TEXT NOT NULL REFERENCES hosts(id),
    internal_port INT NOT NULL,

    CONSTRAINT unique_public_port UNIQUE(public_ip, public_port)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE port_forwards;
DROP TABLE public_ips;
DROP TABLE public_subnets;
DROP TABLE hosts;
DROP TABLE networks;
DROP TABLE routers;
-- +goose StatementEnd
