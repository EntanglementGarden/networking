-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- name: GetRouterByID :one
SELECT * FROM routers WHERE id = ?;

-- name: GetRouterByInstanceID :one
SELECT * FROM routers WHERE instance = ?;

-- name: GetAllRouters :many
SELECT * FROM routers;

-- name: AddRouter :one
INSERT INTO routers (id, instance, trunk_nic, public_gateway) VALUES (?, ?, ?, ?) RETURNING *;

-- name: SetWaitingForRouterBootstrap :exec
UPDATE routers SET post_bootstrap_ip = ? WHERE id = ?;
