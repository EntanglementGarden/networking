-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- name: CreatePublicSubnet :one
INSERT INTO public_subnets (id, subnet) VALUES (?, ?) RETURNING *;

-- name: GetPublicSubnets :many
SELECT * FROM public_subnets;

-- name: GetPublicSubnet :one
SELECT * FROM public_subnets WHERE id = ?;

-- name: AddPublicIP :one
INSERT INTO public_ips (id, ip, subnet, router) VALUES (?, ?, ?, ?) RETURNING *;

-- name: GetPublicIPByID :one
SELECT * FROM public_ips WHERE id = ?;

-- name: GetPublicIPByIP :one
SELECT * FROM public_ips WHERE ip = ?;

-- name: GetPublicIPsBySubnet :many
SELECT * FROM public_ips WHERE subnet = ?;

-- name: GetPublicIPsByRouter :many
SELECT * FROM public_ips WHERE router = ?;
