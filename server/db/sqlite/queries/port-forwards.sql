-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- name: AddPortForward :one
INSERT INTO port_forwards (id, public_ip, public_port, internal_host, internal_port) VALUES (?, ?, ?, ?, ?) RETURNING *;

-- name: GetPortForwards :many
SELECT * FROM port_forwards;

-- name: GetPortForwardsByIP :many
SELECT port_forwards.* FROM port_forwards, public_ips WHERE port_forwards.public_ip = public_ips.id AND public_ips.ip = ?;

-- name: GetPortForwardsByInternalHost :many
SELECT * FROM port_forwards WHERE internal_host = ?;

-- name: GetPortForwardsByRouter :many
SELECT port_forwards.* FROM port_forwards, hosts, networks WHERE port_forwards.internal_host == hosts.id AND hosts.network == networks.id AND networks.router = ?;

-- name: GetRouterForPortForward :one
SELECT networks.router FROM port_forwards, hosts, networks WHERE port_forwards.internal_host == hosts.id AND hosts.network == networks.id AND port_forwards.id = ?;

-- name: DeletePortForward :exec
DELETE FROM port_forwards WHERE id = ?;
