-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- name: CreateSystemNetwork :one
INSERT INTO networks (id, router, tenant, subnet, vlan, egress_ips) VALUES (?, ?, ?, ?, 10, ?) RETURNING *;

-- name: CreateNetwork :one
INSERT INTO networks (id, router, tenant, subnet, vlan, egress_ips) VALUES (?, ?, ?, ?, (SELECT MAX(vlan) + 1 FROM networks), ?) RETURNING *;

-- name: GetNetworksByRouter :many
SELECT * FROM networks WHERE router = ?;
