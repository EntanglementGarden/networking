-- Copyright Entanglement Garden Developers
-- SPDX-License-Identifier: AGPL-3.0-only

-- name: CreateHost :one
INSERT INTO hosts (id, tenant, instance, mac, network, ipv4, trunk, user_data) VALUES (?, ?, ?, ?, ?, ?, ?, ?) RETURNING *;

-- name: GetHost :one
SELECT * FROM hosts WHERE id = ?;

-- name: GetHostByMac :one
SELECT * FROM hosts WHERE mac = ?;

-- name: GetNetworks :many
SELECT * FROM networks WHERE tenant = ?;

-- name: GetNetwork :one
SELECT * FROM networks WHERE id = ?;

-- name: GetHostsByNetwork :many
SELECT * FROM hosts WHERE network = ?;
