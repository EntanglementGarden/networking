// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import "entanglement.garden/networking/server/openapi"

func (s PublicSubnet) GetOpenAPI() openapi.PublicSubnet {
	return openapi.PublicSubnet{
		Id:     s.ID,
		Subnet: s.Subnet,
	}
}
