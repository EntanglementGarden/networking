// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import "entanglement.garden/networking/server/openapi"

func (r Router) GetOpenAPI() openapi.Router {
	return openapi.Router{
		Id:       r.ID,
		Instance: r.Instance,
	}
}
