// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import (
	"context"
	"fmt"
	"net"

	log "github.com/sirupsen/logrus"

	"entanglement.garden/networking/protos"
	"entanglement.garden/networking/server/openapi"
)

func (i PublicIp) GetOpenAPI() openapi.PublicIP {
	return openapi.PublicIP{
		Address: i.Ip,
		Router:  i.Router,
		Subnet:  i.Subnet,
	}
}

func (i PublicIp) GetProto(ctx context.Context, queries *Queries) (*protos.PublicIP, error) {
	dbSubnet, err := queries.GetPublicSubnet(ctx, i.Subnet)
	if err != nil {
		log.Error("error looking up subnet for public IP ", i.Ip)
		return nil, err
	}

	_, subnet, err := net.ParseCIDR(dbSubnet.Subnet)
	if err != nil {
		log.Error("error parsing subnet ", dbSubnet.ID, " CIDR ", dbSubnet.Subnet)
		return nil, err
	}

	router, err := queries.GetRouterByID(ctx, i.Router)
	if err != nil {
		log.Error("error getting router ", i.Router)
		return nil, err
	}

	return &protos.PublicIP{
		Id:      i.ID,
		Address: i.Ip,
		Netmask: fmt.Sprintf("%d.%d.%d.%d", subnet.Mask[0], subnet.Mask[1], subnet.Mask[2], subnet.Mask[3]),
		Gateway: router.PublicGateway,
	}, nil
}
