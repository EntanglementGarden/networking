// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package db

import "entanglement.garden/networking/protos"

func (n Network) GetProto() *protos.Network {
	return &protos.Network{
		ResourceId: n.ID,
		State:      protos.State_ACTIVE,
		VlanId:     int32(n.Vlan),
		Subnet:     n.Subnet,
		EgressIp:   n.EgressIps.String,
		DnsServers: []string{"8.8.8.8", "8.8.4.4", "1.1.1.1"}, // hard-coded for now, eventually should be stored in the DB and exposed to users to select
	}
}
