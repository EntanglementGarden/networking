// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package pki

import (
	"bytes"
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
)

const (
	provisionerPasswordFile = "/var/run/secrets/entanglement.garden/internal-secrets/password"
	rootCA                  = "/var/run/secrets/entanglement.garden/serviceaccount/root_ca.crt"
	caURL                   = "https://step.step-ca.svc.cluster.local:8443"
	stepPath                = "/etc/step"
)

func GetToken(hostname string) (string, error) {
	token, err := runStepCommand("ca", "token", "--root", rootCA, "--ca-url", caURL, "--provisioner", "networking", "--provisioner-password-file", provisionerPasswordFile, hostname)
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(token), nil
}

func GetFingerprint() (string, error) {
	fp, err := runStepCommand("certificate", "fingerprint", rootCA)
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(fp), nil
}

func runStepCommand(args ...string) (string, error) {
	logrus.Debug("executing step ", args)

	output := bytes.Buffer{}
	stderrlog := logrus.StandardLogger().WriterLevel(logrus.WarnLevel)
	defer stderrlog.Close()

	cmd := exec.Command("step", args...)
	// cmd.Env = append(cmd.Env, fmt.Sprintf("STEPPATH=%s", stepPath))
	cmd.Stdout = &output
	cmd.Stderr = stderrlog

	if err := cmd.Run(); err != nil {
		return "", err
	}

	return output.String(), nil
}
