// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package metadata

import (
	"database/sql"
	"net/http"

	"entanglement.garden/networking/server/db"
	"entanglement.garden/networking/server/pki"
	log "github.com/sirupsen/logrus"
)

func StepToken(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	mac := ctx.Value(requesterMacKey).(string)

	queries, dbc, err := db.Get()
	if err != nil {
		log.Error("error getting database: ", err)
		http.Error(w, "error getting token", http.StatusInternalServerError)
		return
	}
	defer dbc.Close()

	host, err := queries.GetHostByMac(ctx, mac)
	if err != nil {
		log.Error("error querying host by mac: ", err)
		if err == sql.ErrNoRows {
			http.Error(w, "not found", http.StatusNotFound)
		} else {
			http.Error(w, "error looking up requester", http.StatusInternalServerError)
		}
		return
	}

	token, err := pki.GetToken(host.Instance)
	if err != nil {
		log.Error("error generating ott token: ", err)
		http.Error(w, "error generating token", http.StatusInternalServerError)
		return
	}

	_, _ = w.Write([]byte(token))
}
