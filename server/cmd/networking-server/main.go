// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"

	"entanglement.garden/networking/server/config"
	"entanglement.garden/networking/server/db"
	"entanglement.garden/networking/server/grpcserver"
	"entanglement.garden/networking/server/httpserver"
	"entanglement.garden/networking/server/routers"
)

func main() {
	config.LoadServerConfig()
	logrus.SetLevel(logrus.TraceLevel)

	if err := db.Migrate(); err != nil {
		logrus.Fatal("error migrating database: ", err)
	}

	go httpserver.Serve()
	go grpcserver.Listen()

	for {
		err := routers.EnsureSystemRouter(context.Background())
		if err != nil {
			logrus.Error("error creating system router: ", err)
			logrus.Info("retrying system router creation in 5 seconds")
			time.Sleep(time.Second * 5)
			continue
		}
		break
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)
	for {
		signal := <-signals
		logrus.Debug("received signal", signal)
		switch signal {
		case syscall.SIGINT, syscall.SIGTERM:
			if err := httpserver.Shutdown(); err != nil {
				logrus.Error("error shutting down local rpc server: ", err)
			}

			grpcserver.Shutdown()
			return
		}
	}
}
