// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package config

import (
	log "github.com/sirupsen/logrus"

	"entanglement.garden/common/config"
	"entanglement.garden/common/grpcx"
	"entanglement.garden/common/installdata"
)

type Config struct {
	HTTPBind                string     `json:"http_listener"`
	GRPCBind                string     `json:"grpc_listener"`
	PKI                     *grpcx.PKI `json:"pki"`
	RouterBaseImage         string     `json:"router_base_image"`
	RouterBaseImageQcow2    string     `json:"router_base_image_qcow2"`
	BootstrapMetadataServer string     `json:"bootstrap_metadata_server"`
	InternalIPRanges        []string   `json:"internal_ip_ranges"`
}

type PublicIP struct {
	Address string `json:"address"`
	Subnet  string `json:"subnet"`
	Gateway string `json:"gateway"`
}

type Host struct {
	ID    string `json:"id"`
	NicID string `json:"nic_id"`
	MAC   string `json:"mac"`
}

var (
	InstallData installdata.InstallData
	C           = Config{
		HTTPBind:             ":8080",
		GRPCBind:             ":9091",
		RouterBaseImage:      "https://git.callpipe.com/entanglement.garden/vm-images/openwrt/-/jobs/artifacts/main/raw/openwrt.img.gz?job=build#compression=gz",
		RouterBaseImageQcow2: "https://git.callpipe.com/entanglement.garden/vm-images/openwrt/-/jobs/artifacts/main/raw/openwrt.qcow2?job=build",
		InternalIPRanges:     []string{"10.0.0.0/8"},
		PKI:                  grpcx.NewServerPKI(),
	}
)

// LoadServerConfig loads the configuration from a JSON file on the disk
func LoadServerConfig() {
	err := config.Load("entanglement.garden", "networking", &C)
	if err != nil {
		log.Fatal("error loading config: ", err)
	}

	InstallData, err = installdata.ReadInstallData()
	if err != nil {
		log.Error("error loading installData: ", err)
	}
}
