#!/bin/bash
set -exuo pipefail
make server/networking-server
podman build -t entanglement-networking -f - . <<EOF
FROM gcr.io/distroless/base-debian11:nonroot
COPY --from=smallstep/step-ca /usr/local/bin/step /usr/bin/step
ADD server/networking-server /networking-server
CMD ["/networking-server"]
EOF

STEP_IP="$(podman inspect step-ca | jq -r '.[0].NetworkSettings.Networks."entanglement-devtools".IPAddress')"
podman run --rm --network entanglement-devtools --env-file "$(pwd)/../devtools/entanglement-common.env" --name networking -p 9091:9090 --add-host "ca.entanglement.garden.localtest.me:${STEP_IP}" -e STEPPATH=/etc/step -v $(pwd)/../devtools/state/networking:/var/entanglement.garden/networking -v $(pwd)/../devtools/state/networking-step:/etc/step -v $(pwd)/../devtools/state/configs:/etc/entanglement.garden entanglement-networking
