#!/bin/bash
version="$(git describe --tags --always)"
branch="${CI_COMMIT_BRANCH:-$(git rev-parse --abbrev-ref HEAD)}"
default_branch="${CI_DEFAULT_BRANCH:-main}"

if [[ "${branch}" == "${default_branch}" || "${branch}" == "HEAD" ]]; then
    echo "${version}"
else
    echo "${version}/${branch}"
fi
