#!/bin/bash
set -exuo pipefail
make networking-server
tag=$(git rev-parse --abbrev-ref HEAD)
image="codeberg.org/EntanglementGarden/networking:${tag}"
podman build -t "${image}" -f - . <<EOF
FROM debian:stable
COPY --from=smallstep/step-ca /usr/local/bin/step /usr/bin/step
ADD networking-server /networking-server
CMD ["/networking-server"]
EOF

podman push "${image}"
