# one-liner to run on the openwrt side to download a new version of the agent. expects host to be running a server on 8000:
# $ python3 -m http.server
# i have been coupling this with automatic rebuild on save:
# $ while inotifywait -e MODIFY -r *; do make; done

/etc/init.d/entanglement-agent stop && wget -O /usr/bin/entanglement-agent http://192.168.100.1:8000/entanglement-agent && chmod +x /usr/bin/entanglement-agent && /etc/init.d/entanglement-agent start && logread -f
