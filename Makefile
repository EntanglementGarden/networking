all: server/networking-server client/entanglement-openwrt-agent

server/networking-server: server/db/db.go server/openapi/openapi.go protos/openwrt.pb.go server/httpserver/web.ewp $(shell find server/ -name '*.go' -print)
	cd server && go build -ldflags "-X entanglement.garden/common/config.Version=$(shell ./version.sh)" ./cmd/networking-server

client/entanglement-openwrt-agent: protos/openwrt.pb.go $(shell find client/ -name '*.go' -print)
	cd client && CGO_ENABLED=0 go build -ldflags "-X entanglement.garden/common/config.Version=$(shell ./version.sh)" ./cmd/entanglement-openwrt-agent

server/db/db.go: server/sqlc.yaml $(shell find server/db/sqlite -name '*.sql' -print)
	cd server && sqlc generate

server/openapi/openapi.go: server/openapi/spec.yaml
	eg dev codegen openapi-server server/openapi/spec.yaml server/openapi/openapi.go

protos/openwrt.pb.go: protos/openwrt.proto
	protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative protos/*.proto

server/httpserver/web.ewp: web/templates/network-create.qtpl.go web/templates/network-get.qtpl.go web/templates/network-list.qtpl.go $(shell find web/ -name '*.go' -print)
	cd web && go build -trimpath -ldflags "-X entanglement.garden/common/config.Version=$(shell ./version.sh)" -o ../server/httpserver/web.ewp

web/templates/*.qtpl.go: $(shell find web/templates -name '*.qtpl' -print)
	qtc -dir web/templates

clean:
	-rm server/networking-server client/entanglement-openwrt-agent server/httpserver/web.ewp
