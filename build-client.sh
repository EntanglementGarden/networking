#!/bin/bash
# Copyright 2021 Entanglement Garden Developers
# SPDX-License-Identifier: AGPL-3.0-only

CONTAINER_NAME="entanglement-networking-build-$(uuidgen)"
msg="* enter to rebuild\\n* ctrl-c to clear cache and exit.\\n* for a shell in the build container: podman exec -ti ${CONTAINER_NAME} bash"
buildcmd="./client/build-ipk.sh |& tee build.log; echo -e \"exit code \$?\\n\\n${msg}\""

touch build.log
chmod a+rw build.log
mkdir -p bin
chmod a+rw bin

(
    echo "set +x"
    echo "${buildcmd}"
    while read _; do
        echo "${buildcmd}"
    done
) | podman run --rm --name "${CONTAINER_NAME}" -i -v "$(pwd):/src" -w /src -i quay.io/openwrt/sdk

echo "exiting"
podman rm "${CONTAINER_NAME}"
