#!/bin/bash
set -exuo pipefail
branch=$(git rev-parse --abbrev-ref HEAD)
image="codeberg.org/entanglementgarden/networking:${branch}"
make clean
make server/networking-server
podman build --build-arg "GOPROXY=${GOPROXY:-direct}" -t "${image}" -f - . <<EOF
FROM smallstep/step-ca AS step

FROM debian:stable
RUN groupadd --gid 65532 networking-server && useradd --uid 65532 --gid 65532 networking-server
COPY --from=step /usr/local/bin/step /usr/bin/step
USER networking-server
ADD server/networking-server /networking-server
CMD ["/networking-server"]
EOF

podman push "${image}"
